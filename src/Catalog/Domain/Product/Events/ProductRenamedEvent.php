<?php


namespace App\Catalog\Domain\Product\Events;


use App\Shared\Domain\Uuid\Uuid;

final class ProductRenamedEvent
{

    public function __construct(public readonly Uuid $productId, public readonly string $newName)
    {
    }
}
