<?php

namespace App\Catalog\Domain\Product\Events;

use App\Shared\Domain\Money\Money;
use App\Shared\Domain\Uuid\Uuid;

final class ProductPriceChangedEvent
{
    public function __construct(public readonly Uuid $productId, public readonly Money $newPrice)
    {
    }

}
