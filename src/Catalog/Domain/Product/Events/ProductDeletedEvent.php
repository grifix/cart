<?php


namespace App\Catalog\Domain\Product\Events;


use App\Shared\Domain\Uuid\Uuid;

final class ProductDeletedEvent
{
    public function __construct(public readonly Uuid $productId)
    {
    }
}
