<?php


namespace App\Catalog\Domain\Product\Events;


use App\Shared\Domain\Money\Money;
use App\Shared\Domain\Uuid\Uuid;

final class ProductCreatedEvent
{
    public function __construct(public readonly Uuid $productId, public readonly string $productName, public readonly Money $price)
    {
    }
}
