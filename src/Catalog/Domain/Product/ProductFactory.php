<?php


namespace App\Catalog\Domain\Product;


use App\Catalog\Domain\Product\Name\Exception\ProductNameAlreadyExistsException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameTooLongException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameTooShortException;
use App\Shared\Domain\Money\Currency\Exceptions\CurrencyIsNotSupportedException;
use App\Shared\Domain\Money\Exceptions\AmountMustBePositiveException;
use App\Shared\Domain\Money\Money;
use App\Shared\Domain\Uuid\Uuid;

final class ProductFactory
{
    private ProductOutsideInterface $outside;

    public function __construct(
        ProductOutsideInterface $outside
    )
    {
        $this->outside = $outside;
    }

    /**
     * @throws ProductNameTooShortException
     * @throws ProductNameTooLongException
     * @throws AmountMustBePositiveException
     * @throws CurrencyIsNotSupportedException
     * @throws ProductNameAlreadyExistsException
     */
    public function create(Uuid $id, string $name, Money $price): Product
    {
        return new Product(
            $this->outside,
            $id,
            $name,
            $price
        );
    }
}
