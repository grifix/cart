<?php

namespace App\Catalog\Domain\Product\Name;

use App\Catalog\Domain\Product\Name\Exception\ProductNameAlreadyExistsException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameTooLongException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameTooShortException;
use App\Catalog\Domain\Product\ProductOutsideInterface;

final class ProductName
{
    /** @dependency */
    private ProductOutsideInterface $outside;

    private string $value;

    /**
     * @throws ProductNameAlreadyExistsException
     * @throws ProductNameTooLongException
     * @throws ProductNameTooShortException
     */
    public function __construct(ProductOutsideInterface $outside, string $value)
    {
        $this->outside = $outside;
        $maxNameLength = $this->outside->getMaxProductNameLength();
        $minNameLength = $this->outside->getMinProductNameLength();
        $nameLength = mb_strlen($value);
        if ($nameLength > $maxNameLength) {
            throw new ProductNameTooLongException($maxNameLength);
        }
        if ($nameLength < $minNameLength) {
            throw new ProductNameTooShortException($minNameLength);
        }

        if ($this->outside->productNameExists($value)) {
            throw new ProductNameAlreadyExistsException($value);
        }
        $this->value = $value;
    }

    /**
     * @throws ProductNameAlreadyExistsException
     * @throws ProductNameTooLongException
     * @throws ProductNameTooShortException
     */
    public function change(string $newName): self
    {
        if ($newName === $this->value) {
            return $this;
        }
        return new self($this->outside, $newName);
    }
}
