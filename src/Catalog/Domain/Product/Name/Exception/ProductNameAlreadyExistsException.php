<?php


namespace App\Catalog\Domain\Product\Name\Exception;


final class ProductNameAlreadyExistsException extends \Exception
{
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
        parent::__construct(sprintf('Product name "%s" already exists!', $name));
    }

    public function getName(): string
    {
        return $this->name;
    }
}
