<?php


namespace App\Catalog\Domain\Product\Name\Exception;

final class ProductNameTooLongException extends \InvalidArgumentException
{
    public function __construct(string $maxChars)
    {
        parent::__construct(sprintf('Product name is too long. It can have maximum %s characters!', $maxChars));
    }
}
