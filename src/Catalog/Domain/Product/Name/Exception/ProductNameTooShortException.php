<?php


namespace App\Catalog\Domain\Product\Name\Exception;

final class ProductNameTooShortException extends \InvalidArgumentException
{
    public function __construct(string $minChars)
    {
        parent::__construct(sprintf('Product name is too short. It must have at least %s characters!', $minChars));
    }
}
