<?php


namespace App\Catalog\Domain\Product\Exceptions;


final class ProductDeletedException extends \LogicException
{
    private string $productId;

    public function __construct(string $productId)
    {
        $this->productId = $productId;
        parent::__construct(sprintf('Product with id "%s" has been deleted!', $productId));
    }

    public function getProductId(): string
    {
        return $this->productId;
    }
}
