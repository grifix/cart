<?php


namespace App\Catalog\Domain\Product;


use App\Shared\Domain\EventPublisherInterface;

interface ProductOutsideInterface extends EventPublisherInterface
{
    public function productNameExists(string $name):bool;

    public function getMaxProductNameLength():int;

    public function getMinProductNameLength():int;
}
