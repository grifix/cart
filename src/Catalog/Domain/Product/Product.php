<?php

declare(strict_types=1);


namespace App\Catalog\Domain\Product;

use App\Catalog\Domain\Product\Events\ProductCreatedEvent;
use App\Catalog\Domain\Product\Events\ProductDeletedEvent;
use App\Catalog\Domain\Product\Events\ProductPriceChangedEvent;
use App\Catalog\Domain\Product\Events\ProductRenamedEvent;
use App\Catalog\Domain\Product\Exceptions\ProductDeletedException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameAlreadyExistsException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameTooLongException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameTooShortException;
use App\Catalog\Domain\Product\Name\ProductName;
use App\Shared\Domain\AggregateRootTrait;
use App\Shared\Domain\Money\Currency\Exceptions\CurrencyIsNotSupportedException;
use App\Shared\Domain\Money\Exceptions\AmountMustBePositiveException;
use App\Shared\Domain\Money\Money;
use App\Shared\Domain\Uuid\Exceptions\InvalidUuidException;
use App\Shared\Domain\Uuid\Uuid;

final class Product
{
    use AggregateRootTrait;

    /** @dependency */
    private ProductOutsideInterface $outside;

    private Uuid $id;

    private Money $price;

    private ProductName $name;

    private bool $deleted = false;

    /**
     * @throws ProductNameAlreadyExistsException
     * @throws InvalidUuidException
     * @throws ProductNameTooShortException
     * @throws ProductNameTooLongException
     * @throws AmountMustBePositiveException
     * @throws CurrencyIsNotSupportedException
     */
    public function __construct(
        ProductOutsideInterface $outside,
        Uuid                    $id,
        string                  $name,
        Money                   $price
    )
    {
        $this->outside = $outside;
        $this->applyProductCreatedEvent(
            new ProductCreatedEvent(
                $id,
                $name,
                $price
            )
        );
    }

    /**
     * @throws ProductNameAlreadyExistsException
     */
    private function applyProductCreatedEvent(ProductCreatedEvent $event): void
    {
        $this->id = $event->productId;
        $this->price = $event->price;
        $this->name = new ProductName($this->outside, $event->productName);
        $this->publishEvent($event);
    }

    /**
     * @throws ProductNameAlreadyExistsException
     * @throws ProductNameTooLongException
     * @throws ProductNameTooShortException
     * @throws ProductDeletedException
     */
    public function rename(string $newName): void
    {
        $this->assertIsNotDeleted();
        $this->applyProductRenamedEvent(new ProductRenamedEvent(
            $this->id,
            $newName
        ));
    }

    /**
     * @throws ProductNameAlreadyExistsException
     */
    private function applyProductRenamedEvent(ProductRenamedEvent $event): void
    {
        $this->name = $this->name->change($event->newName);
        $this->publishEvent($event);
    }

    /**
     * @throws AmountMustBePositiveException
     * @throws ProductDeletedException
     */
    public function changePrice(int|string $newPrice): void
    {
        $this->assertIsNotDeleted();
        $this->applyProductPriceChangedEvent(new ProductPriceChangedEvent($this->id, $this->price->withAmount($newPrice)));
    }

    private function applyProductPriceChangedEvent(ProductPriceChangedEvent $event): void
    {
        $this->price = $event->newPrice;
        $this->publishEvent($event);
    }

    /**
     * @throws ProductDeletedException
     */
    public function delete(): void
    {
        $this->assertIsNotDeleted();
        $this->applyProductDeletedEvent(new ProductDeletedEvent($this->id));
    }

    private function applyProductDeletedEvent(ProductDeletedEvent $event): void
    {
        $this->deleted = true;
        $this->publishEvent($event);
    }

    /**
     * @throws ProductDeletedException
     */
    private function assertIsNotDeleted(): void
    {
        if (true === $this->deleted) {
            throw new ProductDeletedException((string)$this->id);
        }
    }
}
