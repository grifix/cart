<?php


namespace App\Catalog\Domain\Product\Repository\Exceptions;

final class ProductNotExistException extends \Exception
{
    public function __construct(string $productId)
    {
        parent::__construct(sprintf('Product with id %s does not exist!', $productId));
    }
}
