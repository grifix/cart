<?php


namespace App\Catalog\Domain\Product\Repository;


use App\Catalog\Domain\Product\Product;
use App\Catalog\Domain\Product\Repository\Exceptions\ProductNotExistException;

interface ProductRepositoryInterface
{
    /**
     * @throws ProductNotExistException
     */
    public function get(string $id): Product;

    public function add(Product $product): void;

    public function update(Product $product): void;
}
