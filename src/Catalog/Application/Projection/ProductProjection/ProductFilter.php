<?php


namespace App\Catalog\Application\Projection\ProductProjection;


final class ProductFilter
{
    private ?string $productId = null;

    private ?int $offset = null;

    private ?int $limit = null;

    private ?string $name = null;

    private ?string $orderBy = null;

    private ?string $orderDirection = null;

    private function __construct()
    {
    }

    public static function create():self{
        return new self();
    }

    public function getProductId(): ?string
    {
        return $this->productId;
    }

    public function setProductId(?string $productId): ProductFilter
    {
        $this->productId = $productId;
        return $this;
    }

    public function getOffset(): ?int
    {
        return $this->offset;
    }

    public function setOffset(?int $offset): ProductFilter
    {
        $this->offset = $offset;
        return $this;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function setLimit(?int $limit): ProductFilter
    {
        $this->limit = $limit;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ProductFilter
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrderBy(): ?string
    {
        return $this->orderBy;
    }

    /**
     * @param string|null $orderBy
     * @return ProductFilter
     */
    public function setOrderBy(?string $orderBy): ProductFilter
    {
        $this->orderBy = $orderBy;
        return $this;
    }

    public function getOrderDirection(): ?string
    {
        return $this->orderDirection;
    }

    public function setOrderDirection(?string $orderDirection): ProductFilter
    {
        $this->orderDirection = $orderDirection;
        return $this;
    }

}
