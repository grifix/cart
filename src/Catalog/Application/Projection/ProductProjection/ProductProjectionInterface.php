<?php


namespace App\Catalog\Application\Projection\ProductProjection;


use App\Shared\Domain\Money\Money;

interface ProductProjectionInterface
{
    public function addProduct(ProductDto $product): void;

    public function changeProductPrice(string $productId, Money $newPrice): void;

    public function changeProductName(string $productId, string $newName): void;

    public function deleteProduct(string $productId): void;

    /**
     * @return ProductDto[]
     */
    public function find(ProductFilter $filter): array;
}
