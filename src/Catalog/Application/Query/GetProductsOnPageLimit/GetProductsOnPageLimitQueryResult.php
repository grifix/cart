<?php

declare(strict_types=1);

namespace App\Catalog\Application\Query\GetProductsOnPageLimit;


final class GetProductsOnPageLimitQueryResult
{
    private int $limit;

    public function __construct(int $limit)
    {
        $this->limit = $limit;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }
}
