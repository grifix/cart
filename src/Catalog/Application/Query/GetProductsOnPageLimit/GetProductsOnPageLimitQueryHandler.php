<?php

declare(strict_types=1);

namespace App\Catalog\Application\Query\GetProductsOnPageLimit;


use App\Catalog\Application\Dictionary\ProductNameConstraintDictionary\ConstraintsDictionaryInterface;

final class GetProductsOnPageLimitQueryHandler
{
    private ConstraintsDictionaryInterface $constraintDictionary;

    public function __construct(ConstraintsDictionaryInterface $constraintDictionary)
    {
        $this->constraintDictionary = $constraintDictionary;
    }

    public function __invoke(GetProductsOnPageLimitQuery $query): GetProductsOnPageLimitQueryResult
    {
        return new GetProductsOnPageLimitQueryResult($this->constraintDictionary->getMaxProductsOnPage());
    }
}
