<?php


namespace App\Catalog\Application\Query\FindProducts;


use App\Catalog\Application\Projection\ProductProjection\ProductDto;

final class FindProductsQueryResult
{
    /** @var ProductDto[] */
    private array $products;

    /**
     * @param ProductDto[] $products
     */
    public function __construct(array $products)
    {
        $this->products = $products;
    }

    /**
     * @return ProductDto[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }
}
