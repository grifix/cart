<?php


namespace App\Catalog\Application\Query\FindProducts;


use App\Catalog\Application\Projection\ProductProjection\ProductProjectionInterface;

final class FindProductsQueryHandler
{
    private ProductProjectionInterface $productProjection;

    public function __construct(ProductProjectionInterface $productProjection)
    {
        $this->productProjection = $productProjection;
    }

    public function __invoke(FindProductsQuery $query): FindProductsQueryResult
    {
        return new FindProductsQueryResult($this->productProjection->find($query->getFilter()));
    }
}
