<?php


namespace App\Catalog\Application\Query\FindProducts;


use App\Catalog\Application\Projection\ProductProjection\ProductFilter;

final class FindProductsQuery
{
    private ProductFilter $filter;

    public function __construct(ProductFilter $filter)
    {
        $this->filter = $filter;
    }

    public function getFilter(): ProductFilter
    {
        return $this->filter;
    }
}
