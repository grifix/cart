<?php

declare(strict_types=1);

namespace App\Catalog\Application\Query\GetProductNameConstraints;

use App\Catalog\Application\Dictionary\ProductNameConstraintDictionary\ProductNameConstraintsDto;

final class GetProductNameConstraintsQueryResult
{
    private ProductNameConstraintsDto $productNameConstraints;

    public function __construct(ProductNameConstraintsDto $productNameConstraints)
    {
        $this->productNameConstraints = $productNameConstraints;
    }

    public function getProductNameConstraints(): ProductNameConstraintsDto
    {
        return $this->productNameConstraints;
    }
}
