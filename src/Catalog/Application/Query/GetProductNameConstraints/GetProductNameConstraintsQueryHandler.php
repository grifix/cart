<?php

declare(strict_types=1);

namespace App\Catalog\Application\Query\GetProductNameConstraints;


use App\Catalog\Application\Dictionary\ProductNameConstraintDictionary\ConstraintsDictionaryInterface;

final class GetProductNameConstraintsQueryHandler
{
    private ConstraintsDictionaryInterface $constraintsDictionary;

    /**
     * GetProductNameConstraintsQueryHandler constructor.
     * @param ConstraintsDictionaryInterface $constraintsDictionary
     */
    public function __construct(ConstraintsDictionaryInterface $constraintsDictionary)
    {
        $this->constraintsDictionary = $constraintsDictionary;
    }

    public function __invoke(GetProductNameConstraintsQuery $query): GetProductNameConstraintsQueryResult
    {
        return new GetProductNameConstraintsQueryResult($this->constraintsDictionary->getProductNameConstraints());
    }
}
