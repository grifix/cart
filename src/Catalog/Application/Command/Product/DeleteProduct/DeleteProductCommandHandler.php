<?php


namespace App\Catalog\Application\Command\Product\DeleteProduct;


use App\Catalog\Domain\Product\Exceptions\ProductDeletedException;
use App\Catalog\Domain\Product\Repository\Exceptions\ProductNotExistException;
use App\Catalog\Domain\Product\Repository\ProductRepositoryInterface;

final class DeleteProductCommandHandler
{
    private ProductRepositoryInterface $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @throws ProductNotExistException
     * @throws ProductDeletedException
     */
    public function __invoke(DeleteProductCommand $command): void
    {
        $product = $this->productRepository->get($command->getProductId());
        $product->delete();
        $this->productRepository->update($product);
    }
}
