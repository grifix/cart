<?php


namespace App\Catalog\Application\Command\Product\DeleteProduct;


final class DeleteProductCommand
{
    private string $productId;

    public function __construct(string $productId)
    {
        $this->productId = $productId;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }
}
