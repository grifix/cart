<?php


namespace App\Catalog\Application\Command\Product\ChangeProductPrice;


final class ChangeProductPriceCommand
{
    private string $productId;

    private int $newPrice;

    public function __construct(string $productId, int $newPrice)
    {
        $this->productId = $productId;
        $this->newPrice = $newPrice;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function getNewPrice(): int
    {
        return $this->newPrice;
    }
}
