<?php


namespace App\Catalog\Application\Command\Product\ChangeProductPrice;


use App\Catalog\Domain\Product\Repository\Exceptions\ProductNotExistException;
use App\Catalog\Domain\Product\Repository\ProductRepositoryInterface;
use App\Shared\Domain\Money\Exceptions\AmountMustBePositiveException;

final class ChangeProductPriceCommandHandler
{
    private ProductRepositoryInterface $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @throws ProductNotExistException
     * @throws AmountMustBePositiveException
     */
    public function __invoke(ChangeProductPriceCommand $command): void
    {
        $product = $this->productRepository->get($command->getProductId());
        $product->changePrice($command->getNewPrice());
        $this->productRepository->update($product);
    }
}
