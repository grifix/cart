<?php


namespace App\Catalog\Application\Command\Product\RenameProduct;


final class RenameProductCommand
{
    private string $productId;

    private string $newName;

    public function __construct(string $productId, string $newName)
    {
        $this->productId = $productId;
        $this->newName = $newName;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function getNewName(): string
    {
        return $this->newName;
    }
}
