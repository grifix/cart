<?php

namespace App\Catalog\Application\Command\Product\RenameProduct;

use App\Catalog\Domain\Product\Name\Exception\ProductNameAlreadyExistsException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameTooLongException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameTooShortException;
use App\Catalog\Domain\Product\Repository\Exceptions\ProductNotExistException;
use App\Catalog\Domain\Product\Repository\ProductRepositoryInterface;

final class RenameProductCommandHandler
{
    private ProductRepositoryInterface $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param RenameProductCommand $command
     * @throws ProductNameAlreadyExistsException
     * @throws ProductNotExistException
     * @throws ProductNameTooLongException
     * @throws ProductNameTooShortException
     */
    public function __invoke(RenameProductCommand $command): void
    {
        $product = $this->productRepository->get($command->getProductId());
        $product->rename($command->getNewName());
        $this->productRepository->update($product);
    }
}
