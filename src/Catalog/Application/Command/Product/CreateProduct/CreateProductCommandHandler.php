<?php
declare(strict_types=1);

namespace App\Catalog\Application\Command\Product\CreateProduct;


use App\Catalog\Domain\Product\Name\Exception\ProductNameAlreadyExistsException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameTooLongException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameTooShortException;
use App\Catalog\Domain\Product\ProductFactory;
use App\Catalog\Domain\Product\Repository\ProductRepositoryInterface;
use App\Shared\Domain\Money\Currency\Exceptions\CurrencyIsNotSupportedException;
use App\Shared\Domain\Money\Exceptions\AmountMustBePositiveException;
use App\Shared\Domain\Uuid\UuidFactory;

final class CreateProductCommandHandler
{
    private ProductRepositoryInterface $productRepository;

    private ProductFactory $productFactory;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        ProductFactory             $productFactory,
        private UuidFactory        $uuidFactory
    )
    {
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
    }

    /**
     * @throws ProductNameTooShortException
     * @throws ProductNameTooLongException
     * @throws AmountMustBePositiveException
     * @throws CurrencyIsNotSupportedException
     * @throws ProductNameAlreadyExistsException
     */
    public function __invoke(CreateProductCommand $command): void
    {
        $this->productRepository->add(
            $this->productFactory->create(
                $this->uuidFactory->create($command->getId()),
                $command->getName(),
                $command->getPrice()
            )
        );
    }
}
