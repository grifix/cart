<?php

declare(strict_types=1);

namespace App\Catalog\Application\Dictionary\ProductNameConstraintDictionary;


interface ConstraintsDictionaryInterface
{
    public function getProductNameConstraints(): ProductNameConstraintsDto;

    public function getMaxProductsOnPage():int;
}
