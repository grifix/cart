<?php

declare(strict_types=1);

namespace App\Catalog\Application\Dictionary\ProductNameConstraintDictionary;


final class ProductNameConstraintsDto
{
    private int $maxLength;

    private int $minLength;

    public function __construct(int $maxLength, int $minLength)
    {
        $this->maxLength = $maxLength;
        $this->minLength = $minLength;
    }

    /**
     * @return int
     */
    public function getMaxLength(): int
    {
        return $this->maxLength;
    }

    /**
     * @return int
     */
    public function getMinLength(): int
    {
        return $this->minLength;
    }
}
