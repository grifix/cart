<?php


namespace App\Catalog\Application\Listener;


use App\Catalog\Application\Projection\ProductProjection\ProductDto;
use App\Catalog\Application\Projection\ProductProjection\ProductProjectionInterface;
use App\Catalog\Domain\Product\Events\ProductCreatedEvent;
use App\Catalog\Domain\Product\Events\ProductDeletedEvent;
use App\Catalog\Domain\Product\Events\ProductPriceChangedEvent;
use App\Catalog\Domain\Product\Events\ProductRenamedEvent;
use App\Shared\Application\AbstractListener;

final class ProductProjectorListener extends AbstractListener
{
    private ProductProjectionInterface $productProjection;

    public function __construct(ProductProjectionInterface $productProjector)
    {
        $this->productProjection = $productProjector;
    }

    public function onCreateProduct(ProductCreatedEvent $event): void
    {
        $this->productProjection->addProduct(
            new ProductDto(
                $event->productId,
                $event->productName,
                $event->price
            )
        );
    }

    public function onDeleteProduct(ProductDeletedEvent $event): void
    {
        $this->productProjection->deleteProduct($event->productId);
    }

    public function onChangeProductPrice(ProductPriceChangedEvent $event): void
    {
        $this->productProjection->changeProductPrice($event->productId, $event->newPrice);
    }

    public function onRenameProduct(ProductRenamedEvent $event): void
    {
        $this->productProjection->changeProductName($event->productId, $event->newName);
    }
}
