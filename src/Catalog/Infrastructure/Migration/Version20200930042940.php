<?php

declare(strict_types=1);

namespace App\Catalog\Infrastructure\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200930042940 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Product projection';
    }

    public function up(Schema $schema) : void
    {
        $sql = <<<SQL
create table catalog.product_projection
(
	id uuid
		constraint product_projection_pk
			primary key,
	name varchar,
	price_amount int not null,
	price_currency varchar not null,
	number bigserial
);
SQL;
        $this->addSql($sql);

        $sql = <<<SQL
create unique index product_projection_name_uindex
	on catalog.product_projection (name);
SQL;
        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('drop table catalog.product_projection');
    }
}
