<?php

declare(strict_types=1);

namespace App\Catalog\Infrastructure\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200925035930 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Catalog aggregate store';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE SCHEMA catalog');
        $sql = <<<SQL
create table catalog.product
(
	id uuid
		constraint product_pk
			primary key,
	data jsonb not null
)
SQL;
        $this->addSql($sql);

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('drop table catalog.product');
        $this->addSql('drop schema catalog');

    }
}
