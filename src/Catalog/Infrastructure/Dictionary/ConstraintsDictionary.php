<?php

declare(strict_types=1);

namespace App\Catalog\Infrastructure\Dictionary;


use App\Catalog\Application\Dictionary\ProductNameConstraintDictionary\ConstraintsDictionaryInterface;
use App\Catalog\Application\Dictionary\ProductNameConstraintDictionary\ProductNameConstraintsDto;

final class ConstraintsDictionary implements ConstraintsDictionaryInterface
{
    private int $productNameMaxLength;

    private int $productNameMinLength;

    private int $maxProductsOnPage;

    public function __construct(
        int $productNameMaxLength,
        int $productNameMinLength,
        int $maxProductsOnPage
    )
    {
        $this->productNameMaxLength = $productNameMaxLength;
        $this->productNameMinLength = $productNameMinLength;
        $this->maxProductsOnPage = $maxProductsOnPage;
    }

    public function getProductNameConstraints(): ProductNameConstraintsDto
    {
        return new ProductNameConstraintsDto($this->productNameMaxLength, $this->productNameMinLength);
    }

    public function getMaxProductsOnPage(): int
    {
        return $this->maxProductsOnPage;
    }
}
