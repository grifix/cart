<?php


namespace App\Catalog\Infrastructure\Domain\Product;


use App\Catalog\Application\Dictionary\ProductNameConstraintDictionary\ConstraintsDictionaryInterface;
use App\Catalog\Domain\Product\ProductOutsideInterface;
use App\Shared\Application\ContextClient\CatalogService\CatalogClientInterface;
use App\Shared\Infrastructure\Common\EventCollector\EventCollectorInterface;
use App\Shared\Infrastructure\Domain\EventPublisherTrait;

final class ProductOutside implements ProductOutsideInterface
{
    use EventPublisherTrait;


    public function __construct(
        protected EventCollectorInterface      $eventCollector,
        private ConstraintsDictionaryInterface $constraintsDictionary,
        private CatalogClientInterface         $catalogClient
    )
    {
    }

    public function productNameExists(string $name): bool
    {
        return $this->catalogClient->productNameExists($name);
    }

    public function getMaxProductNameLength(): int
    {
        return $this->constraintsDictionary->getProductNameConstraints()->getMaxLength();
    }

    public function getMinProductNameLength(): int
    {
        return $this->constraintsDictionary->getProductNameConstraints()->getMinLength();
    }

}
