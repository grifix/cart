<?php
declare(strict_types=1);

namespace App\Catalog\Infrastructure\Domain\Product;

use App\Catalog\Domain\Product\Product;
use App\Catalog\Domain\Product\Repository\Exceptions\ProductNotExistException;
use App\Catalog\Domain\Product\Repository\ProductRepositoryInterface;
use App\Catalog\Infrastructure\Common\Doctrine\Entity\ProductEntity;
use App\Shared\Infrastructure\Domain\AbstractRepository;

final class DoctrineProductRepository extends AbstractRepository implements ProductRepositoryInterface
{
    public function get(string $id): Product
    {
        /** @var Product|null $result */
        $result = $this->doFind($id);
        if (null === $result) {
            throw new ProductNotExistException($id);
        }
        return $result;
    }

    public function add(Product $product): void
    {
        $this->doAdd($product);
    }

    protected function getEntityClass(): string
    {
        return ProductEntity::class;
    }

    public function update(Product $product):void{
        $this->doUpdate($product);
    }
}
