<?php


namespace App\Catalog\Infrastructure\Appliction\Projection;


use App\Catalog\Application\Projection\ProductProjection\ProductDto;
use App\Catalog\Application\Projection\ProductProjection\ProductFilter;
use App\Catalog\Application\Projection\ProductProjection\ProductProjectionInterface;
use App\Shared\Domain\Money\Money;
use App\Shared\Domain\Money\MoneyDto;
use App\Shared\Domain\Money\MoneyFactory;
use Doctrine\DBAL\Connection;

final class ProductProjection implements ProductProjectionInterface
{
    private const TABLE = 'catalog.product_projection';

    private Connection $connection;

    public function __construct(
        Connection $connection,
        private MoneyFactory $moneyFactory
    )
    {
        $this->connection = $connection;
    }

    public function addProduct(ProductDto $product): void
    {
        $this->connection->insert(
            self::TABLE,
            [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'price_amount' => $product->getPrice()->getAmount(),
                'price_currency' => $product->getPrice()->getCurrencyCode()
            ]
        );
    }

    public function changeProductPrice(string $productId, Money $newPrice): void
    {
        $this->connection->update(self::TABLE,
            [
                'price_amount' => $newPrice->getAmount()
            ],
            ['id' => $productId]
        );
    }

    public function changeProductName(string $productId, string $newName): void
    {
        $this->connection->update(self::TABLE,
            [
                'name' => $newName
            ],
            ['id' => $productId]
        );
    }

    public function deleteProduct(string $productId): void
    {
        $this->connection->delete(self::TABLE, ['id' => $productId]);
    }

    public function find(ProductFilter $filter): array
    {
        $qb = $this->connection->createQueryBuilder();
        $qb->select('*')->from('catalog.product_projection');
        if (null !== $filter->getName()) {
            $qb->where('name = :name')->setParameter('name', $filter->getName());
        }
        if (null !== $filter->getProductId()) {
            $qb->where('id = :id')->setParameter('id', $filter->getProductId());
        }
        if (null !== $filter->getLimit()) {
            $qb->setMaxResults($filter->getLimit());
        }
        if (null !== $filter->getOffset()) {
            $qb->setFirstResult($filter->getOffset());
        }
        if (null !== $filter->getOrderBy()) {
            $qb->orderBy($filter->getOrderBy(), $filter->getOrderDirection());
        }
        $records = $this->connection->executeQuery($qb->getSQL(), $qb->getParameters())->fetchAllAssociative();
        $result = [];
        foreach ($records as $record) {
            $result[] = new ProductDto(
                $record['id'],
                $record['name'],
                $this->moneyFactory->create(
                    $record['price_amount'],
                    $record['price_currency']
                )
            );
        }
        return $result;
    }
}
