<?php


namespace App\Catalog\Infrastructure\Common\Doctrine\Entity;

use App\Shared\Infrastructure\Common\Doctrine\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Catalog\Infrastructure\Domain\Product\DoctrineProductRepository")
 * @ORM\Table(name="catalog.product")
 */
class ProductEntity extends AbstractEntity
{

}
