<?php

declare(strict_types=1);

namespace App\Catalog\Ui\Validator;

use App\Catalog\Application\Query\GetProductNameConstraints\GetProductNameConstraintsQuery;
use App\Shared\Application\QueryBusInterface;
use App\Shared\Ui\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\Positive;

final class UpdateProductValidator implements ValidatorInterface
{
    private QueryBusInterface $queryBus;

    public function __construct(QueryBusInterface $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function getConstraints(): Collection
    {
        $productNameConstraints = $this->queryBus->executeQuery(new GetProductNameConstraintsQuery())->getProductNameConstraints();

        return new Collection([
            'fields' => [
                'name' => new Optional([
                    new NotBlank(),
                    new Length([
                        'min' => $productNameConstraints->getMinLength(),
                        'max' => $productNameConstraints->getMaxLength()
                    ])
                ]),
                'price' => new Optional([
                    new Collection([
                        'amount' => new Optional([
                            new NotBlank(),
                            new Positive()
                        ])
                    ])
                ])
            ]
        ]);
    }
}
