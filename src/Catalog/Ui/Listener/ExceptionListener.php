<?php

declare(strict_types=1);

namespace App\Catalog\Ui\Listener;


use App\Catalog\Domain\Product\Exceptions\ProductDeletedException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameAlreadyExistsException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameTooLongException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameTooShortException;
use App\Catalog\Domain\Product\Repository\Exceptions\ProductNotExistException;
use App\Shared\Domain\Money\Currency\Exceptions\CurrencyIsNotSupportedException;
use App\Shared\Domain\Money\Exceptions\AmountMustBePositiveException;
use App\Shared\Infrastructure\Common\ExceptionResponder\ExceptionResponder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ControllerEvent;

final class ExceptionListener
{
    private ExceptionResponder $exceptionResponder;

    public function __construct(ExceptionResponder $exceptionResponder)
    {
        $this->exceptionResponder = $exceptionResponder;
    }

    public function onKernelController(ControllerEvent $event): void
    {
        $this->exceptionResponder->addException(ProductNameAlreadyExistsException::class, Response::HTTP_BAD_REQUEST,
            function (ProductNameAlreadyExistsException $exception) {
                return sprintf('Product with name %s already exists!', $exception->getName());
            })
            ->addException(ProductNameTooLongException::class)
            ->addException(ProductNameTooShortException::class)
            ->addException(AmountMustBePositiveException::class)
            ->addException(CurrencyIsNotSupportedException::class)
            ->addException(ProductNotExistException::class, Response::HTTP_NOT_FOUND)
            ->addException(ProductDeletedException::class, Response::HTTP_NOT_FOUND,
                function (ProductDeletedException $exception) {
                    return sprintf('Product with id %s does not exist!', $exception->getProductId());
                });
    }
}
