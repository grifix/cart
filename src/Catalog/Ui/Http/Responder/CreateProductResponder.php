<?php


namespace App\Catalog\Ui\Http\Responder;


use App\Catalog\Application\Command\Product\CreateProduct\CreateProductCommand;
use App\Catalog\Ui\Validator\CreateProductValidator;
use App\Shared\Domain\Money\Money;
use App\Shared\Ui\Http\Responder\AbstractResponder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class CreateProductResponder extends AbstractResponder
{
    public function __invoke(Request $request): JsonResponse
    {
        $payload = $this->getPayload($request);
        $this->validate($payload->getArray(),CreateProductValidator::class);

        $id = $this->getUuid();
        $this->executeCommand(new CreateProductCommand(
            $id,
            $payload->get('name'),
            $this->createMoney(
                $payload->get('price.amount'),
                $payload->get('price.currency')
            )
        ));

        return new JsonResponse(['id' => $id], Response::HTTP_CREATED);
    }
}
