<?php

declare(strict_types=1);

namespace App\Catalog\Ui\Http\Responder;


use App\Catalog\Application\Command\Product\ChangeProductPrice\ChangeProductPriceCommand;
use App\Catalog\Application\Command\Product\RenameProduct\RenameProductCommand;
use App\Catalog\Ui\Validator\UpdateProductValidator;
use App\Shared\Ui\Http\Responder\AbstractResponder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class UpdateProductResponder extends AbstractResponder
{

    public function __invoke(Request $request): JsonResponse
    {
        $payload = $this->getPayload($request);
        $this->validate($payload->getArray(), UpdateProductValidator::class);
        if($payload->has('name')){
            $this->executeCommand(new RenameProductCommand($request->get('productId'), $payload->get('name')));
        }
        if($payload->has('price.amount')){
            $this->executeCommand(new ChangeProductPriceCommand($request->get('productId'), (int) $payload->get('price.amount')));
        }
        return new JsonResponse(null, Response::HTTP_OK);
    }
}
