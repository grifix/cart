<?php

declare(strict_types=1);

namespace App\Catalog\Ui\Http\Responder;


use App\Catalog\Application\Command\Product\DeleteProduct\DeleteProductCommand;
use App\Shared\Ui\Http\Responder\AbstractResponder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class DeleteProductResponder extends AbstractResponder
{

    public function __invoke(Request $request): Response
    {
        $this->executeCommand(new DeleteProductCommand($request->get('productId')));
        return new JsonResponse(null, Response::HTTP_OK);
    }
}
