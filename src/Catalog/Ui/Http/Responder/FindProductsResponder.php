<?php


namespace App\Catalog\Ui\Http\Responder;


use App\Catalog\Application\Projection\ProductProjection\ProductFilter;
use App\Catalog\Application\Query\FindProducts\FindProductsQuery;
use App\Catalog\Application\Query\GetProductsOnPageLimit\GetProductsOnPageLimitQuery;
use App\Shared\Ui\Http\Responder\AbstractResponder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class FindProductsResponder extends AbstractResponder
{

    public function __invoke(Request $request): Response
    {
        $productsOnPage = $this->executeQuery(new GetProductsOnPageLimitQuery())->getLimit();
        $page = $request->get('page') ?? 1;
        $offset = $productsOnPage * $page - $productsOnPage;
        $products = $this->executeQuery(
            new FindProductsQuery(
                ProductFilter::create()
                    ->setOrderBy('number')
                    ->setOrderDirection('ASC')
                    ->setOffset($offset)
                    ->setLimit($productsOnPage)
            )
        )->getProducts();

        $result = [];
        foreach ($products as $product) {
            $arr = $this->toArray($product);
            $arr['price']['amount'] = $product->getPrice()->getAmount()/100;
            $result[] = $arr;
        }

        return new JsonResponse($result);
    }
}
