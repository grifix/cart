<?php
declare(strict_types=1);

namespace App\Cart\Domain\Cart;


use App\Shared\Domain\Uuid\Uuid;

final class CartFactory
{
    private CartOutsideInterface $outside;

    public function __construct(CartOutsideInterface $outside)
    {
        $this->outside = $outside;
    }

    public function create(Uuid $id): Cart
    {
        return new Cart($this->outside, $id);
    }
}
