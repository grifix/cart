<?php


namespace App\Cart\Domain\Cart\Repository\Exception;

final class CartNotExistException extends \Exception
{
    public function __construct(string $cartId)
    {
        parent::__construct(sprintf('Cart "%s" does not exist!', $cartId));
    }
}
