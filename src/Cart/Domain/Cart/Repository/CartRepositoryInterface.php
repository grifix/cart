<?php


namespace App\Cart\Domain\Cart\Repository;


use App\Cart\Domain\Cart\Cart;
use App\Cart\Domain\Cart\Repository\Exception\CartNotExistException;

interface CartRepositoryInterface
{
    /**
     * @throws CartNotExistException
     */
    public function get(string $id): Cart;

    public function add(Cart $cart): void;

    public function update(Cart $cart): void;
}
