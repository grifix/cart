<?php
declare(strict_types=1);

namespace App\Cart\Domain\Cart\CartItem;


use App\Shared\Domain\Money\Money;
use App\Shared\Domain\Uuid\Uuid;

final class CartItem
{
    public function __construct(public readonly Uuid $productId, public readonly Money $price)
    {
    }
}
