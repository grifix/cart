<?php

declare(strict_types=1);

namespace App\Cart\Domain\Cart;

use App\Cart\Domain\Cart\CartItem\CartItem;
use App\Cart\Domain\Cart\Events\CartCreatedEvent;
use App\Cart\Domain\Cart\Events\ProductAddedToCartEvent;
use App\Cart\Domain\Cart\Events\ProductRemovedFromCartEvent;
use App\Cart\Domain\Cart\Exceptions\CartItemsLimitExceedException;
use App\Cart\Domain\Cart\Exceptions\ProductAlreadyAddedToCartException;
use App\Cart\Domain\Cart\Exceptions\ProductNotExistInCartException;
use App\Shared\Domain\AggregateRootTrait;
use App\Shared\Domain\Money\Money;
use App\Shared\Domain\Uuid\Uuid;

final class Cart
{
    use AggregateRootTrait;

    /** @dependency */
    private CartOutsideInterface $outside;

    private Uuid $id;

    /** @var CartItem[] */
    private array $items = [];

    private Money $totalPrice;

    public function __construct(
        CartOutsideInterface $outside,
        Uuid                 $id
    )
    {
        $this->outside = $outside;
        $this->applyCartCreatedEvent(
            new CartCreatedEvent($id, $this->outside->createMoney()),
        );
    }

    private function applyCartCreatedEvent(CartCreatedEvent $event): void
    {
        $this->id = $event->cartId;
        $this->totalPrice = $event->totalPrice;
        $this->publishEvent($event);
    }

    /**
     * @throws ProductAlreadyAddedToCartException
     * @throws CartItemsLimitExceedException
     */
    public function addProduct(Uuid $productId): void
    {
        $itemsLimit = $this->outside->getItemsLimit();
        if (count($this->items) >= $itemsLimit) {
            throw new CartItemsLimitExceedException((string)$this->id, $itemsLimit);
        }
        $this->assertProductNotAlreadyAdded($productId);

        $productPrice = $this->outside->getProductPrice($productId);
        $newTotalPrice = $this->totalPrice->add($this->outside->getProductPrice($productId));

        $this->applyProductAddedToCartEvent(new ProductAddedToCartEvent(
            $this->id,
            $productId,
            $productPrice,
            $newTotalPrice
        ));
    }

    private function applyProductAddedToCartEvent(ProductAddedToCartEvent $event): void
    {
        $this->totalPrice = $event->newTotalPrice;
        $this->items[] = new CartItem($event->productId, $event->productPrice);
        $this->publishEvent($event);
    }

    /**
     * @throws ProductNotExistInCartException
     */
    public function removeProduct(Uuid $productId): void
    {
        $this->assertHasProduct($productId);
        $newTotalPrice = $this->totalPrice->subtract($this->getItemPrice($productId));
        $this->applyProductRemovedFromCartEvent(new ProductRemovedFromCartEvent(
            $this->id,
            $productId,
            $newTotalPrice
        ));
    }

    private function applyProductRemovedFromCartEvent(ProductRemovedFromCartEvent $event): void
    {
        $this->totalPrice = $event->newTotalPrice;
        foreach ($this->items as $i => $item){
            if($item->productId->isEqualTo($event->productId)){
                unset($this->items[$i]);
                break;
            }
        }
        $this->publishEvent($event);
    }

    private function hasProduct(Uuid $productId): bool
    {
        foreach ($this->items as $item) {
            if ($item->productId->isEqualTo($productId)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @throws ProductAlreadyAddedToCartException
     */
    private function assertProductNotAlreadyAdded(Uuid $productId): void
    {
        if ($this->hasProduct($productId)) {
            throw new ProductAlreadyAddedToCartException((string)$this->id, (string)$productId);
        }
    }

    /**
     * @throws ProductNotExistInCartException
     */
    private function assertHasProduct(Uuid $productId): void
    {
        if (!$this->hasProduct($productId)) {
            throw new ProductNotExistInCartException((string)$this->id, (string)$productId);
        }
    }

    private function getItemPrice(Uuid $productId): Money
    {
        foreach ($this->items as $item) {
            if ($item->productId->isEqualTo($productId)) {
                return $item->price;
            }
        }
        throw new ProductNotExistInCartException((string)$this->id, (string)$productId);
    }
}
