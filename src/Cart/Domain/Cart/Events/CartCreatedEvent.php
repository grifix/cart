<?php
declare(strict_types=1);

namespace App\Cart\Domain\Cart\Events;


use App\Shared\Domain\Money\Money;
use App\Shared\Domain\Uuid\Uuid;

final class CartCreatedEvent
{

    public function __construct(
        public readonly Uuid $cartId,
        public readonly Money $totalPrice
    )
    {
    }
}
