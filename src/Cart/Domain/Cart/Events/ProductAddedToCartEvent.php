<?php
declare(strict_types=1);

namespace App\Cart\Domain\Cart\Events;


use App\Shared\Domain\Money\Money;
use App\Shared\Domain\Uuid\Uuid;

final class ProductAddedToCartEvent
{

    public function __construct(
        public readonly Uuid $cartId,
        public readonly Uuid $productId,
        public readonly Money $productPrice,
        public readonly Money $newTotalPrice
    )
    {
    }
}
