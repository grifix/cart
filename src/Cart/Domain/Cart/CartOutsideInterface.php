<?php


namespace App\Cart\Domain\Cart;


use App\Shared\Domain\EventPublisherInterface;
use App\Shared\Domain\Money\Money;
use App\Shared\Domain\Uuid\Uuid;

interface CartOutsideInterface extends EventPublisherInterface
{
    public function getItemsLimit(): int;

    public function getProductPrice(Uuid $productId): Money;

    public function createMoney(): Money;
}
