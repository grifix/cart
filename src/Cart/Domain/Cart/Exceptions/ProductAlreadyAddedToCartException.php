<?php
declare(strict_types=1);

namespace App\Cart\Domain\Cart\Exceptions;


final class ProductAlreadyAddedToCartException extends \LogicException
{
    public function __construct(string $cartId, string $productId)
    {
        parent::__construct(sprintf('Product "%s" had been already added to the cart "%s"!', $productId, $cartId));
    }
}
