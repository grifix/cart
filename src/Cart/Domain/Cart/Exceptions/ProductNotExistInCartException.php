<?php
declare(strict_types=1);

namespace App\Cart\Domain\Cart\Exceptions;

final class ProductNotExistInCartException extends \LogicException
{
    public function __construct(string $cartId, string $productId)
    {
        parent::__construct(sprintf('Product "%s" does not exist in the cart "%s"!', $productId, $cartId));
    }
}
