<?php
declare(strict_types=1);

namespace App\Cart\Domain\Cart\Exceptions;


use LogicException;

final class CartItemsLimitExceedException extends LogicException
{
    public function __construct(string $cartId, int $itemLimit)
    {
        parent::__construct(sprintf('Items limit (%s) for cart "%s" is exceed!', $itemLimit, $cartId));
    }
}
