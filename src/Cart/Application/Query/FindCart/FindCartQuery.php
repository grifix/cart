<?php

declare(strict_types=1);

namespace App\Cart\Application\Query\FindCart;


final class FindCartQuery
{
    private string $cartId;

    public function __construct(string $cartId)
    {
        $this->cartId = $cartId;
    }

    public function getCartId(): string
    {
        return $this->cartId;
    }
}
