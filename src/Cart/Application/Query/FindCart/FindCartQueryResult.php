<?php

declare(strict_types=1);

namespace App\Cart\Application\Query\FindCart;


use App\Cart\Application\Projection\CartProjection\CartDto;

final class FindCartQueryResult
{
    private ?CartDto $cart = null;

    public function __construct(?CartDto $cart = null)
    {
        $this->cart = $cart;
    }

    public function getCart(): ?CartDto
    {
        return $this->cart;
    }
}
