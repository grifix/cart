<?php

declare(strict_types=1);

namespace App\Cart\Application\Query\FindCart;


use App\Cart\Application\Projection\CartProjection\CartProjectionInterface;

final class FindCartQueryHandler
{
    private CartProjectionInterface $cartProjection;

    public function __construct(CartProjectionInterface $cartProjection)
    {
        $this->cartProjection = $cartProjection;
    }


    public function __invoke(FindCartQuery $query): FindCartQueryResult
    {
        return new FindCartQueryResult($this->cartProjection->findCart($query->getCartId()));
    }
}
