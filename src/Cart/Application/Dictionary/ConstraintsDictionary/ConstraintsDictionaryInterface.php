<?php

declare(strict_types=1);

namespace App\Cart\Application\Dictionary\ConstraintsDictionary;


interface ConstraintsDictionaryInterface
{
    public function getMaxCartItemsLimit(): int;
}
