<?php

namespace App\Cart\Application\Listener;

use App\Cart\Application\Projection\CartProjection\CartDto;
use App\Cart\Application\Projection\CartProjection\CartProjectionInterface;
use App\Cart\Application\Projection\CartProjection\CartItemDto;
use App\Cart\Domain\Cart\Events\CartCreatedEvent;
use App\Cart\Domain\Cart\Events\ProductAddedToCartEvent;
use App\Cart\Domain\Cart\Events\ProductRemovedFromCartEvent;
use App\Shared\Application\AbstractListener;
use App\Shared\Application\ContextClient\CatalogService\CatalogClientInterface;

final class CartProjectionListener extends AbstractListener
{
    private CartProjectionInterface $cartProjection;

    private CatalogClientInterface $catalogClient;

    public function __construct(CartProjectionInterface $cartProjection, CatalogClientInterface $catalogClient)
    {
        $this->cartProjection = $cartProjection;
        $this->catalogClient = $catalogClient;
    }

    public function onCartCreated(CartCreatedEvent $event)
    {
        $this->cartProjection->addCart(new CartDto($event->cartId, [], 0));
    }

    public function onProductAddedToCart(ProductAddedToCartEvent $event)
    {
        $product = $this->catalogClient->findProduct($event->productId);
        $this->cartProjection->addProduct(
            $event->cartId,
            new CartItemDto($product->getId(), $product->getName(), $product->getPrice()),
            (int) $event->newTotalPrice->getAmount()
        );
    }

    public function onProductRemovedFromCart(ProductRemovedFromCartEvent $event)
    {
        $this->cartProjection->removeProduct($event->cartId, $event->productId, (int) $event->newTotalPrice->getAmount());
    }
}
