<?php


namespace App\Cart\Application\Projection\CartProjection;


interface CartProjectionInterface
{
    public function addCart(CartDto $cart):void;

    public function addProduct(string $cartId, CartItemDto $product, int $total):void;

    public function removeProduct(string $cartId, string $productId, int $total):void;

    public function findCart(string $cartId):?CartDto;
}
