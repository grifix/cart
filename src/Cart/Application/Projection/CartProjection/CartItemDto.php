<?php


namespace App\Cart\Application\Projection\CartProjection;


use App\Shared\Domain\Money\Money;

final class CartItemDto
{
    private string $productId;

    private string $name;

    private Money $price;

    public function __construct(string $productId, string $name, Money $price)
    {
        $this->productId = $productId;
        $this->name = $name;
        $this->price = $price;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): Money
    {
        return $this->price;
    }
}
