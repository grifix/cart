<?php


namespace App\Cart\Application\Projection\CartProjection;


final class CartDto
{
    private string $id;

    /** @var CartItemDto[] */
    private array $items = [];

    private int $total;

    public function __construct(string $id, array $items, int $total)
    {
        $this->id = $id;
        $this->items = $items;
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return CartItemDto[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }
}
