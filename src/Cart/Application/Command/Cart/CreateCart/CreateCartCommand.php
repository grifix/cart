<?php


namespace App\Cart\Application\Command\Cart\CreateCart;


final class CreateCartCommand
{
    public function __construct(private string $id)
    {
    }

    public function getId(): string
    {
        return $this->id;
    }
}
