<?php


namespace App\Cart\Application\Command\Cart\CreateCart;


use App\Cart\Domain\Cart\CartFactory;
use App\Cart\Domain\Cart\Repository\CartRepositoryInterface;
use App\Shared\Domain\Uuid\UuidFactory;

final class CreateCartCommandHandler
{
    private CartRepositoryInterface $cartRepository;

    private CartFactory $cartFactory;

    public function __construct(
        CartRepositoryInterface $cartRepository,
        CartFactory             $cartFactory,
        private UuidFactory     $uuidFactory
    )
    {
        $this->cartRepository = $cartRepository;
        $this->cartFactory = $cartFactory;
    }

    public function __invoke(CreateCartCommand $command): void
    {
        $this->cartRepository->add(
            $this->cartFactory->create(
                $this->uuidFactory->create($command->getId())
            )
        );
    }

}
