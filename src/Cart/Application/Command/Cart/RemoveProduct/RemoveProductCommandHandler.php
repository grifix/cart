<?php


namespace App\Cart\Application\Command\Cart\RemoveProduct;


use App\Cart\Domain\Cart\Exceptions\ProductNotExistInCartException;
use App\Cart\Domain\Cart\Repository\CartRepositoryInterface;
use App\Cart\Domain\Cart\Repository\Exception\CartNotExistException;
use App\Shared\Domain\Uuid\UuidFactory;

final class RemoveProductCommandHandler
{
    private CartRepositoryInterface $cartRepository;

    public function __construct(
        CartRepositoryInterface $cartRepository,
        private UuidFactory $uuidFactory
    )
    {
        $this->cartRepository = $cartRepository;
    }

    /**
     * @throws CartNotExistException
     * @throws ProductNotExistInCartException
     */
    public function __invoke(RemoveProductCommand $command): void
    {
        $cart = $this->cartRepository->get($command->getCartId());
        $cart->removeProduct($this->uuidFactory->create($command->getProductId()));
        $this->cartRepository->update($cart);
    }
}
