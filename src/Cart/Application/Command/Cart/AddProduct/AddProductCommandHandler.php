<?php


namespace App\Cart\Application\Command\Cart\AddProduct;


use App\Cart\Domain\Cart\Exceptions\CartItemsLimitExceedException;
use App\Cart\Domain\Cart\Exceptions\ProductAlreadyAddedToCartException;
use App\Cart\Domain\Cart\Repository\CartRepositoryInterface;
use App\Cart\Domain\Cart\Repository\Exception\CartNotExistException;
use App\Shared\Domain\Uuid\UuidFactory;

final class AddProductCommandHandler
{
    private CartRepositoryInterface $cartRepository;

    public function __construct(
        CartRepositoryInterface $cartRepository,
        private UuidFactory $uuidFactory
    )
    {
        $this->cartRepository = $cartRepository;
    }

    /**
     * @throws CartNotExistException
     * @throws ProductAlreadyAddedToCartException
     * @throws CartItemsLimitExceedException
     */
    public function __invoke(AddProductCommand $command):void{
       $cart = $this->cartRepository->get($command->getCartId());
       $cart->addProduct($this->uuidFactory->create($command->getProductId()));
       $this->cartRepository->update($cart);
    }

}
