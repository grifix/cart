<?php


namespace App\Cart\Application\Command\Cart\AddProduct;


final class AddProductCommand
{
    private string $cartId;

    private string $productId;

    public function __construct(string $cartId, string $productId)
    {
        $this->cartId = $cartId;
        $this->productId = $productId;
    }

    public function getCartId(): string
    {
        return $this->cartId;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }
}
