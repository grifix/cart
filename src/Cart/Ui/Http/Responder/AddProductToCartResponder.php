<?php

declare(strict_types=1);

namespace App\Cart\Ui\Http\Responder;


use App\Cart\Application\Command\Cart\AddProduct\AddProductCommand;
use App\Cart\Ui\Validator\AddProductToCartValidator;
use App\Shared\Ui\Http\Responder\AbstractResponder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class AddProductToCartResponder extends AbstractResponder
{

    public function __invoke(Request $request): Response
    {
        $payload = $this->getPayload($request);
        $this->validate($payload->getArray(), AddProductToCartValidator::class);
        $this->executeCommand(new AddProductCommand($request->get('cartId'), $payload->get('productId')));
        return new JsonResponse(null, Response::HTTP_CREATED);
    }
}
