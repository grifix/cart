<?php

declare(strict_types=1);

namespace App\Cart\Ui\Http\Responder;


use App\Cart\Application\Command\Cart\RemoveProduct\RemoveProductCommand;
use App\Shared\Ui\Http\Responder\AbstractResponder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class RemoveProductFromCartResponder extends AbstractResponder
{

    public function __invoke(Request $request): Response
    {
        $this->executeCommand(new RemoveProductCommand($request->get('cartId'), $request->get('productId')));
        return new JsonResponse(null, JsonResponse::HTTP_OK);
    }
}
