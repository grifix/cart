<?php

declare(strict_types=1);

namespace App\Cart\Ui\Http\Responder;


use App\Cart\Application\Command\Cart\CreateCart\CreateCartCommand;
use App\Shared\Ui\Http\Responder\AbstractResponder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class CreateCartResponder extends AbstractResponder
{

    public function __invoke(Request $request): Response
    {
        $id = $this->getUuid();
        $this->executeCommand(new CreateCartCommand($id));
        return new JsonResponse(['id' => $id], Response::HTTP_CREATED);
    }
}
