<?php

declare(strict_types=1);

namespace App\Cart\Ui\Listener;


use App\Cart\Domain\Cart\Exceptions\CartItemsLimitExceedException;
use App\Cart\Domain\Cart\Exceptions\ProductAlreadyAddedToCartException;
use App\Cart\Domain\Cart\Exceptions\ProductNotExistInCartException;
use App\Cart\Domain\Cart\Repository\Exception\CartNotExistException;
use App\Shared\Infrastructure\Common\ExceptionResponder\ExceptionResponder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ControllerEvent;

final class ExceptionListener
{
    private ExceptionResponder $exceptionResponder;

    public function __construct(ExceptionResponder $exceptionResponder)
    {
        $this->exceptionResponder = $exceptionResponder;
    }

    public function onKernelController(ControllerEvent $event): void
    {
        $this->exceptionResponder
            ->addException(CartItemsLimitExceedException::class)
            ->addException(ProductAlreadyAddedToCartException::class)
            ->addException(ProductNotExistInCartException::class, Response::HTTP_NOT_FOUND)
            ->addException(CartNotExistException::class, Response::HTTP_NOT_FOUND)
        ;
    }
}
