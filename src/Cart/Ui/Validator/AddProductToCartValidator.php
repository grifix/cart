<?php

declare(strict_types=1);

namespace App\Cart\Ui\Validator;

use App\Shared\Ui\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Uuid;

final class AddProductToCartValidator implements ValidatorInterface
{

    public function getConstraints(): Collection
    {
        return new Collection([
            'fields' => [
                'productId' => new Required([
                    new NotBlank(),
                    new Uuid()
                ]),
            ]
        ]);
    }
}
