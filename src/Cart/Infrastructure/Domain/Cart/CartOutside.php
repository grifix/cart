<?php


namespace App\Cart\Infrastructure\Domain\Cart;


use App\Cart\Application\Dictionary\ConstraintsDictionary\ConstraintsDictionaryInterface;
use App\Cart\Domain\Cart\CartOutsideInterface;
use App\Cart\Domain\Cart\Item\CartItemFactoryInterface;
use App\Cart\Domain\Cart\Item\CartItemInterface;
use App\Shared\Application\ContextClient\CatalogService\CatalogClientInterface;
use App\Shared\Domain\Money\Currency\Currency;
use App\Shared\Domain\Money\Money;
use App\Shared\Domain\Money\MoneyFactory;
use App\Shared\Domain\Uuid\Uuid;
use App\Shared\Domain\Uuid\UuidFactoryInterface;
use App\Shared\Domain\Uuid\UuidInterface;
use App\Shared\Infrastructure\Common\EventCollector\EventCollectorInterface;
use App\Shared\Infrastructure\Domain\EventPublisherTrait;

final class CartOutside implements CartOutsideInterface
{
    use EventPublisherTrait;

    public function __construct(
        protected ConstraintsDictionaryInterface $constraintDictionary,
        protected EventCollectorInterface        $eventCollector,
        private CatalogClientInterface           $catalogClient,
        private MoneyFactory                     $moneyFactory
    )
    {
    }

    public function getItemsLimit(): int
    {
        return $this->constraintDictionary->getMaxCartItemsLimit();
    }

    public function getProductPrice(Uuid $productId): Money
    {
        return $this->catalogClient->findProduct($productId)->getPrice();
    }

    public function createMoney(): Money
    {
        return $this->moneyFactory->create(0, Currency::CODE_PLN);
    }
}
