<?php


namespace App\Cart\Infrastructure\Domain\Cart;


use App\Cart\Domain\Cart\Cart;
use App\Cart\Domain\Cart\Repository\CartRepositoryInterface;
use App\Cart\Domain\Cart\Repository\Exception\CartNotExistException;
use App\Cart\Infrastructure\Common\Doctrine\Entity\CartEntity;
use App\Shared\Infrastructure\Domain\AbstractRepository;

final class DoctrineCartRepository extends AbstractRepository implements CartRepositoryInterface
{

    public function get(string $id): Cart
    {
        /** @var Cart|null $result */
        $result = $this->doFind($id);
        if (null === $result) {
            throw new CartNotExistException($id);
        }
        return $result;
    }

    public function add(Cart $cart): void
    {
        $this->doAdd($cart);
    }

    public function update(Cart $cart): void
    {
        $this->doUpdate($cart);
    }

    protected function getEntityClass(): string
    {
        return CartEntity::class;
    }
}
