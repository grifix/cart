<?php

declare(strict_types=1);

namespace App\Cart\Infrastructure\Common\Doctrine\Entity;


use App\Shared\Infrastructure\Common\Doctrine\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="\App\Cart\Infrastructure\Domain\Cart\CartRepository\DoctrineCartRepository")
 * @ORM\Table(name="cart.cart")
 */
final class CartEntity extends AbstractEntity
{

}
