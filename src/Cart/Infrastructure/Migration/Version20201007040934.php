<?php

declare(strict_types=1);

namespace App\Cart\Infrastructure\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201007040934 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $sql = <<<SQL
create table cart.cart_projection
(
	id uuid
		constraint cart_projection_pk
			primary key,
	total int default 0 not null,
	items jsonb not null
);
SQL;
        $this->addSql($sql);


    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP table cart.cart_projection');
    }
}
