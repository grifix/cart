<?php

declare(strict_types=1);

namespace App\Cart\Infrastructure\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201007040657 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE SCHEMA cart');
        $sql = <<<SQL
create table cart.cart
(
	id uuid
		constraint cart_pk
			primary key,
	data jsonb not null
)
SQL;
        $this->addSql($sql);

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('drop table cart.cart');
        $this->addSql('drop schema cart');

    }
}
