<?php


namespace App\Cart\Infrastructure\Application\Projection;


use App\Cart\Application\Projection\CartProjection\CartDto;
use App\Cart\Application\Projection\CartProjection\CartProjectionInterface;
use App\Cart\Application\Projection\CartProjection\Exception\CartNotExistException;
use App\Cart\Application\Projection\CartProjection\CartItemDto;
use App\Shared\Domain\Money\MoneyFactory;
use Doctrine\DBAL\Connection;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

final class CartProjection implements CartProjectionInterface
{
    private const TABLE = 'cart.cart_projection';

    private Connection $connection;

    private ObjectNormalizer $normalizer;

    /**
     * CartProjection constructor.
     * @param Connection $connection
     */
    public function __construct(
        Connection $connection,
        ObjectNormalizer $normalizer,
        private MoneyFactory $moneyFactory
    )
    {
        $this->connection = $connection;
        $this->normalizer = $normalizer;
        $this->normalizer->setSerializer(new Serializer([$normalizer]));
    }

    public function addCart(CartDto $cart): void
    {
        $this->connection->insert(self::TABLE, [
            'id' => $cart->getId(),
            'items' => '[]'
        ]);
    }

    public function addProduct(string $cartId, CartItemDto $product, int $total): void
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE)->where('id = :id')
            ->setParameter('id', $cartId);
        $cart = $this->connection->executeQuery($query->getSQL(), $query->getParameters())->fetchAssociative();
        $items = json_decode($cart['items'], true);
        $items[] = $this->normalizer->normalize($product);
        $this->connection->update(
            self::TABLE,
            [
                'items' => json_encode($items),
                'total' => $total
            ],
            [
                'id' => $cartId
            ]
        );
    }

    public function removeProduct(string $cartId, string $productId, int $total): void
    {
        $query = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE)->where('id = :id')
            ->setParameter('id', $cartId);
        $cart = $this->connection->executeQuery($query->getSQL(), $query->getParameters())->fetchAssociative();
        $items = json_decode($cart['items'], true);
        foreach ($items as $i=>$item){
            if($item['productId'] === $productId){
                unset($items[$i]);
            }
        }
        $this->connection->update(
            self::TABLE,
            [
                'items' => json_encode($items),
                'total' => $total
            ],
            [
                'id' => $cartId
            ]
        );
    }

    public function findCart(string $cartId): ?CartDto
    {
        $qb = $this->connection->createQueryBuilder()
            ->select('*')
            ->from(self::TABLE)
            ->where('id = :cartId')
            ->setParameter('cartId', $cartId);
        $raw = $this->connection->fetchAssociative($qb->getSQL(), $qb->getParameters());
        if (!$raw) {
            return null;
        }
        $items = json_decode($raw['items'], true);
        foreach ($items as $i => $item) {
            $items[$i] = new CartItemDto(
                $item['productId'],
                $item['name'],
                $this->moneyFactory->create(
                    $item['price']['amount'],
                    $item['price']['currencyCode']
                )

            );

        }
        return new CartDto($raw['id'], $items, $raw['total']);
    }
}
