<?php

declare(strict_types=1);

namespace App\Cart\Infrastructure\Application\Dictionary;

use App\Cart\Application\Dictionary\ConstraintsDictionary\ConstraintsDictionaryInterface;

final class ConstraintDictionary implements ConstraintsDictionaryInterface
{

    private int $maxCartItemsLimit;

    public function __construct(int $maxCartItemsLimit)
    {
        $this->maxCartItemsLimit = $maxCartItemsLimit;
    }


    public function getMaxCartItemsLimit(): int
    {
        return $this->maxCartItemsLimit;
    }
}
