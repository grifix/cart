<?php


namespace App\Shared\Domain;


use App\Shared\Domain\Uuid\UuidInterface;

/**
 * @property UuidInterface $id
 * @property EventPublisherInterface $outside
 */
trait AggregateRootTrait
{
    protected function publishEvent(object $event):void{
        $this->outside->publishEvent($event, get_class($this), (string) $this->id);
    }
}
