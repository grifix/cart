<?php


namespace App\Shared\Domain;


interface EventPublisherInterface
{
    public function publishEvent(object $event, string $aggregateType, string $aggregateId): void;
}
