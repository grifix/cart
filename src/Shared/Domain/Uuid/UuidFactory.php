<?php


namespace App\Shared\Domain\Uuid;


use Ramsey\Uuid\Uuid as RamseyUuid;

final class UuidFactory
{
    private UuidOutsideInterface $outside;

    public function __construct(UuidOutsideInterface $outside)
    {
        $this->outside = $outside;
    }

    public function create(string $value): Uuid
    {
        return new Uuid($this->outside, $value);
    }

    public function createRandom():Uuid{
        return new Uuid($this->outside, RamseyUuid::uuid4());
    }
}
