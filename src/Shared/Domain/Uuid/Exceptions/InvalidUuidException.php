<?php

declare(strict_types=1);


namespace App\Shared\Domain\Uuid\Exceptions;

class InvalidUuidException extends \InvalidArgumentException
{

    public function __construct(string $uuid)
    {
        parent::__construct(sprintf('Uuid "%s" is invalid!', $uuid));
    }
}
