<?php

declare(strict_types=1);


namespace App\Shared\Domain\Uuid;

use App\Shared\Domain\Uuid\Exceptions\InvalidUuidException;

final class Uuid
{
    private string $value;

    /** @dependency */
    private UuidOutsideInterface $outside;

    public function __construct(UuidOutsideInterface $outside, string $value)
    {
        if (false === $outside->isValidUuid($value)) {
            throw new InvalidUuidException($value);
        }
        $this->value = $value;
        $this->outside = $outside;
    }

    public function __toString(): string
    {
        return $this->value;
    }

    public function isEqualTo(Uuid $uuid): bool
    {
        return (string) $uuid === $this->value;
    }
}
