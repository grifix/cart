<?php
declare(strict_types=1);

namespace App\Shared\Domain\Money\Currency;


final class CurrencyFactory
{
    public function __construct(private CurrencyOutsideInterface $outside)
    {
    }

    public function create(string $currencyCode): Currency
    {
        return new Currency($this->outside, $currencyCode);
    }
}
