<?php

declare(strict_types=1);


namespace App\Shared\Domain\Money\Currency;

interface CurrencyOutsideInterface
{
    public function isCurrencySupported(string $currencyCode): bool;
}
