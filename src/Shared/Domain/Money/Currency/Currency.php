<?php

declare(strict_types=1);


namespace App\Shared\Domain\Money\Currency;

use App\Shared\Domain\Money\Currency\Exceptions\CurrencyIsNotSupportedException;

final class Currency
{
    public const CODE_USD = 'USD';
    public const CODE_PLN = 'PLN';

    private string $code;

    /** @dependency */
    private CurrencyOutsideInterface $outside;

    /**
     * @throws CurrencyIsNotSupportedException
     */
    public function __construct(CurrencyOutsideInterface $outside, string $code)
    {
        $this->outside = $outside;
        if (false === $this->outside->isCurrencySupported($code)) {
            throw new CurrencyIsNotSupportedException($code);
        }
        $this->code = $code;
    }

    public function __toString(): string
    {
        return $this->code;
    }

    public function isEqualTo(Currency $other)
    {
        return $this->code === $other->code;
    }
}
