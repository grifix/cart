<?php
declare(strict_types=1);

namespace App\Shared\Domain\Money;


use App\Shared\Domain\Money\Currency\CurrencyFactory;

final class MoneyFactory
{

    public function __construct(private CurrencyFactory $currencyFactory)
    {
    }

    public function create(int|string $amount, string $currencyCode): Money
    {
        return new Money($amount, $this->currencyFactory->create($currencyCode));
    }
}
