<?php

declare(strict_types=1);


namespace App\Shared\Domain\Money;

use App\Shared\Domain\Money\Currency\Currency;
use App\Shared\Domain\Money\Exceptions\AmountMustBePositiveException;

final class Money
{
    private string $amount;

    private Currency $currency;

    /**
     * @throws AmountMustBePositiveException
     */
    public function __construct(string|int $amount, Currency $currency)
    {
        $this->setAmount((string)$amount);
        $this->currency = $currency;
    }

    public function __toString(): string
    {
        return sprintf('%s %s', $this->amount, $this->currency);
    }

    /**
     * @throws AmountMustBePositiveException
     */
    public function withAmount(string|int $amount): self
    {
        $result = clone $this;
        $result->setAmount((string)$amount);
        return $result;
    }

    /**
     * @throws AmountMustBePositiveException
     */
    private function setAmount(string $amount): void
    {
        if (bccomp('0', $amount) === 1) {
            throw new AmountMustBePositiveException();
        }
        $this->amount = $amount;
    }

    public function add(Money $other): self
    {
        return new self(bcadd($this->amount, $other->amount), $this->currency);
    }

    public function subtract(Money $other): self
    {
        return new self(bcsub($this->amount, $other->amount), $this->currency);
    }

    public function isEqualTo(Money $other): bool
    {
        if (false === $other->currency->isEqualTo($this->currency)) {
            return false;
        }

        if ($other->amount !== $this->amount) {
            return false;
        }

        return true;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function getCurrencyCode(): string
    {
        return (string)$this->currency;
    }
}
