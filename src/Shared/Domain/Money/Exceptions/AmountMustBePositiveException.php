<?php

declare(strict_types=1);


namespace App\Shared\Domain\Money\Exceptions;

final class AmountMustBePositiveException extends \InvalidArgumentException
{
    public function __construct()
    {
        parent::__construct(sprintf('Amount must be positive'));
    }
}
