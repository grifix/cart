<?php

namespace App\Shared\Ui\Listener;

use App\Shared\Domain\Money\Currency\Exceptions\CurrencyIsNotSupportedException;
use App\Shared\Domain\Money\Exceptions\AmountMustBePositiveException;
use App\Shared\Infrastructure\Common\ExceptionResponder\ExceptionResponder;
use App\Shared\Infrastructure\Common\RequestDecoder\Exception\MalformedJsonException;
use App\Shared\Ui\Exception\ValidationException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Validator\ConstraintViolationInterface;

final class ExceptionListener
{
    private LoggerInterface $logger;

    private string $env;

    private bool $debugErrors;

    private ExceptionResponder $exceptionResponder;

    public function __construct(
        LoggerInterface $logger,
        ExceptionResponder $exceptionResponder,
        bool $debugErrors,
        string $env
    )
    {
        $this->logger = $logger;
        $this->env = $env;
        $this->exceptionResponder = $exceptionResponder;
        $this->exceptionResponder->addException(MalformedJsonException::class, Response::HTTP_BAD_REQUEST);
        $this->debugErrors = $debugErrors;
        $this->exceptionResponder->addException(AmountMustBePositiveException::class);
        $this->exceptionResponder->addException(CurrencyIsNotSupportedException::class);
    }

    public function onKernelException(ExceptionEvent $event):void
    {
        $exception = $event->getThrowable();
        $response = $this->exceptionResponder->handle($exception);
        if(null === $response && $exception instanceof ValidationException){
            $response = $this->handleValidationException($exception);
        }
        if(null === $response){
            $payload = [
                'message' => 'Internal server error'
            ];
            if(in_array($this->env, ['test', 'dev'])){
                $payload['debug'] = [
                    'exception' => get_class($exception),
                    'message' => $exception->getMessage(),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'code' => $exception->getCode(),
                    'trace' => $exception->getTraceAsString()
                ];
            }
            $response = new JsonResponse($payload, JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            $this->logger->critical($exception->getMessage(), [
                'trace' => $exception->getTraceAsString()
            ]);
        }
        else{
            $this->logger->warning($event->getThrowable()->getMessage(), [
                'content' => (string) $event->getRequest()->getContent(),
                'clientIp' => $event->getRequest()->getClientIp(),
                'method' => $event->getRequest()->getMethod(),
                'uri' => $event->getRequest()->getRequestUri(),
                'trace' => $event->getThrowable()->getTraceAsString()
            ]);
        }

        if($this->debugErrors){
            throw $event->getThrowable();
        }
        $event->setResponse($response);
    }

    private function handleValidationException(ValidationException $exception):JsonResponse{
        $result = [];
        foreach ($exception->getViolationList() as $violation){
            /** @var ConstraintViolationInterface $violation */
            $result[$violation->getPropertyPath()] = $violation->getMessage();
        }
        return new JsonResponse($result, Response::HTTP_BAD_REQUEST);
    }
}
