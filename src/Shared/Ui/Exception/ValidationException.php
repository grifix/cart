<?php

declare(strict_types=1);

namespace App\Shared\Ui\Exception;


use Symfony\Component\Validator\ConstraintViolationListInterface;

final class ValidationException extends \Exception
{
    private ConstraintViolationListInterface $violationList;

    public function __construct(ConstraintViolationListInterface $violationList)
    {
        $this->violationList = $violationList;
        parent::__construct('Validation exception');
    }

    public function getViolationList(): ConstraintViolationListInterface
    {
        return $this->violationList;
    }
}
