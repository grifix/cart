<?php


namespace App\Shared\Ui\Http\Responder;


use App\Shared\Application\CommandBusInterface;
use App\Shared\Application\Query\GetUuid\GetUuidQuery;
use App\Shared\Application\Query\GetUuid\GetUuidQueryResult;
use App\Shared\Application\QueryBusInterface;
use App\Shared\Domain\Money\Money;
use App\Shared\Domain\Money\MoneyFactory;
use App\Shared\Infrastructure\Common\ArrayAccessor;
use App\Shared\Infrastructure\Common\ExceptionResponder\ExceptionResponder;
use App\Shared\Infrastructure\Common\JsonSerializer;
use App\Shared\Infrastructure\Common\RequestDecoder\RequestDecoder;
use App\Shared\Ui\Exception\ValidationException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractResponder
{
    private QueryBusInterface $queryBus;

    private CommandBusInterface $commandBus;

    private RequestDecoder $requestDecoder;

    private ExceptionResponder $exceptionResponder;

    private ValidatorInterface $validator;

    private ContainerInterface $container;

    private PropertyAccessor $propertyAccessor;

    private JsonSerializer $jsonSerializer;

    public function __construct(
        QueryBusInterface    $queryBus,
        CommandBusInterface  $commandBus,
        RequestDecoder       $requestDecoder,
        ExceptionResponder   $exceptionResponder,
        ValidatorInterface   $validator,
        ContainerInterface   $container,
        JsonSerializer       $jsonSerializer,
        private MoneyFactory $moneyFactory
    )
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
        $this->requestDecoder = $requestDecoder;
        $this->exceptionResponder = $exceptionResponder;
        $this->validator = $validator;
        $this->container = $container;
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
        $this->jsonSerializer = $jsonSerializer;
    }

    abstract public function __invoke(Request $request): Response;

    protected function executeQuery(object $query)
    {
        return $this->queryBus->executeQuery($query);
    }

    protected function executeCommand(object $command): void
    {
        $this->commandBus->executeCommand($command);
    }

    protected function getPayload(Request $request): ArrayAccessor
    {
        return new ArrayAccessor($this->requestDecoder->decode($request));
    }

    protected function getUuid(): string
    {
        /** @var GetUuidQueryResult $result */
        $result = $this->executeQuery(new GetUuidQuery());
        return $result->getUuid();
    }

    protected function addExceptionResponse(string $exceptionClass, int $code = null, $message = null): void
    {
        $this->exceptionResponder->addException($exceptionClass, $code, $message);
    }

    protected function validate(array $data, string $validatorClass): void
    {
        /** @var \App\Shared\Ui\Validator\ValidatorInterface $validator */
        $validator = $this->container->get($validatorClass);
        $violationList = $this->validator->validate($data, $validator->getConstraints());
        if ($violationList->count() > 0) {
            throw new ValidationException($violationList);
        }
    }

    protected function createMoney(string|int $amount, string $currency): Money
    {
        return $this->moneyFactory->create($amount, $currency);
    }


    protected function toJson(object $object): string
    {
        return $this->jsonSerializer->serialize($object);
    }

    protected function toArray(object $object): array
    {
        return json_decode($this->toJson($object), true);
    }
}
