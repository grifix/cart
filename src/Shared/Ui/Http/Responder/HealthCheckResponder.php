<?php


namespace App\Shared\Ui\Http\Responder;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class HealthCheckResponder extends AbstractResponder
{
    public function __invoke(Request $request): Response
    {
        return new JsonResponse(['status' => 'ok'], JsonResponse::HTTP_OK);
    }
}
