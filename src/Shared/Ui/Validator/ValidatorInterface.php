<?php

declare(strict_types=1);

namespace App\Shared\Ui\Validator;


use Symfony\Component\Validator\Constraints\Collection;

interface ValidatorInterface
{
    public function getConstraints():Collection;
}
