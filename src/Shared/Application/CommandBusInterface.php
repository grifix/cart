<?php


namespace App\Shared\Application;


interface CommandBusInterface
{
    public function executeCommand($command):void;
}
