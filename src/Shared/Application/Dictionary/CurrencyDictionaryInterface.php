<?php

declare(strict_types=1);

namespace App\Shared\Application\Dictionary;


interface CurrencyDictionaryInterface
{
    public function getCurrencies():array;
}
