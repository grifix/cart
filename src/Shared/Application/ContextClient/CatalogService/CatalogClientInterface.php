<?php


namespace App\Shared\Application\ContextClient\CatalogService;

use App\Shared\Application\ContextClient\CatalogService\Dto\CreateProductDto;
use App\Shared\Application\ContextClient\CatalogService\Dto\ProductDto;
use App\Shared\Application\ContextClient\CatalogService\Exception\ProductNotFoundException;

interface CatalogClientInterface
{
    /**
     * @throws ProductNotFoundException
     */
    public function findProduct(string $productId): ProductDto;

    public function createProduct(CreateProductDto $createProduct): string;

    public function productNameExists(string $productId):bool;
}
