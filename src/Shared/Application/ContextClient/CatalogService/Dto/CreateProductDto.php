<?php


namespace App\Shared\Application\ContextClient\CatalogService\Dto;


use App\Shared\Domain\Money\MoneyDto;

final class CreateProductDto
{
    private string $name;

    private MoneyDto $price;

    public function __construct(string $name, MoneyDto $price)
    {
        $this->name = $name;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return MoneyDto
     */
    public function getPrice(): MoneyDto
    {
        return $this->price;
    }
}
