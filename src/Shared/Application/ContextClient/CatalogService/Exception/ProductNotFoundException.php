<?php


namespace App\Shared\Application\ContextClient\CatalogService\Exception;


final class ProductNotFoundException extends \Exception
{
    public function __construct(string $productId)
    {
        parent::__construct(sprintf('Product "%s" not found!', $productId));
    }
}
