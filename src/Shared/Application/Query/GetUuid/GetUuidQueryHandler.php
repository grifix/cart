<?php


namespace App\Shared\Application\Query\GetUuid;


use Ramsey\Uuid\Uuid;

final class GetUuidQueryHandler
{
    public function __invoke(GetUuidQuery $query):GetUuidQueryResult
    {
        return new GetUuidQueryResult(Uuid::uuid4()->toString());
    }
}
