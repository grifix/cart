<?php


namespace App\Shared\Application\Query\GetUuid;


final class GetUuidQueryResult
{
    private string $uuid;

    public function __construct(string $uuid)
    {
        $this->uuid = $uuid;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }
}
