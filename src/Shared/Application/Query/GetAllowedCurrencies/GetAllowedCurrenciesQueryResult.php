<?php

declare(strict_types=1);

namespace App\Shared\Application\Query\GetAllowedCurrencies;


final class GetAllowedCurrenciesQueryResult
{
    /**
     * @var string[]
     */
    private array $currencies;

    /**
     * GetAllowedCurrenciesQueryResult constructor.
     * @param string[] $currencies
     */
    public function __construct(array $currencies)
    {
        $this->currencies = $currencies;
    }

    /**
     * @return string[]
     */
    public function getCurrencies(): array
    {
        return $this->currencies;
    }


}
