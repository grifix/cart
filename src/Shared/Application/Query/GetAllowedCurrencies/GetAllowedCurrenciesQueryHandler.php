<?php

declare(strict_types=1);

namespace App\Shared\Application\Query\GetAllowedCurrencies;


use App\Shared\Application\Dictionary\CurrencyDictionaryInterface;

final class GetAllowedCurrenciesQueryHandler
{
    private CurrencyDictionaryInterface $currencyDictionary;

    public function __construct(CurrencyDictionaryInterface $currencyDictionary)
    {
        $this->currencyDictionary = $currencyDictionary;
    }

    public function __invoke(GetAllowedCurrenciesQuery $query): GetAllowedCurrenciesQueryResult
    {
        return new GetAllowedCurrenciesQueryResult($this->currencyDictionary->getCurrencies());
    }
}
