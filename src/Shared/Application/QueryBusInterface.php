<?php


namespace App\Shared\Application;


interface QueryBusInterface
{
    public function executeQuery($query);
}
