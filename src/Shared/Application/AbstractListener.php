<?php


namespace App\Shared\Application;


use Symfony\Component\Messenger\Handler\MessageSubscriberInterface;

abstract class AbstractListener implements MessageSubscriberInterface
{

    public static function getHandledMessages(): iterable
    {
        $result = [];
        $reflection = new \ReflectionClass(static::class);
        foreach ($reflection->getMethods(\ReflectionMethod::IS_PUBLIC) as $method){
            if($method->isConstructor() || $method->getNumberOfParameters() <= 0 || $method->getNumberOfParameters() > 1){
                continue;
            }
            $parameter = $method->getParameters()[0];
            $result[$parameter->getType()->getName()]=$method->getName();
        }
        return $result;
    }
}
