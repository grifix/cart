<?php


namespace App\Shared\Infrastructure\Domain;


use App\Shared\Infrastructure\Common\EventCollector\EventCollectorInterface;
use App\Shared\Infrastructure\Common\EventCollector\EventEnvelope;

trait EventPublisherTrait
{
    protected EventCollectorInterface $eventCollector;

    public function publishEvent(object $event, string $aggregateType, string $aggregateId): void
    {
        $this->eventCollector->addEvent(new EventEnvelope($aggregateId, $aggregateType, $event));
    }
}
