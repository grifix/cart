<?php


namespace App\Shared\Infrastructure\Domain\Uuid;


use App\Shared\Domain\Uuid\UuidOutsideInterface;
use Ramsey\Uuid\Uuid;

final class UuidOutside implements UuidOutsideInterface
{

    public function isValidUuid(string $uuid): bool
    {
        return Uuid::isValid($uuid);
    }
}
