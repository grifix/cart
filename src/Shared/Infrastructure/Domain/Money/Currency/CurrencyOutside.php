<?php


namespace App\Shared\Infrastructure\Domain\Money\Currency;


use App\Shared\Application\Dictionary\CurrencyDictionaryInterface;
use App\Shared\Domain\Money\Currency\CurrencyOutsideInterface;

final class CurrencyOutside implements CurrencyOutsideInterface
{
    private CurrencyDictionaryInterface $currencyDictionary;

    public function __construct(CurrencyDictionaryInterface $currencyDictionary)
    {
        $this->currencyDictionary = $currencyDictionary;
    }


    public function isCurrencySupported(string $currencyCode): bool
    {
        return in_array($currencyCode, $this->currencyDictionary->getCurrencies());
    }
}
