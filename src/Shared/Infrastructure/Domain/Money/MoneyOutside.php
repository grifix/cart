<?php


namespace App\Shared\Infrastructure\Domain\Money;


use App\Shared\Domain\Money\Currency\CurrencyFactoryInterface;
use App\Shared\Domain\Money\Currency\CurrencyInterface;
use App\Shared\Domain\Money\MoneyOutsideInterface;

final class MoneyOutside implements MoneyOutsideInterface
{
    private CurrencyFactoryInterface $currencyFactory;

    public function __construct(CurrencyFactoryInterface $currencyFactory)
    {
        $this->currencyFactory = $currencyFactory;
    }

    public function createCurrency(string $currencyCode): CurrencyInterface
    {
        return $this->currencyFactory->create($currencyCode);
    }
}
