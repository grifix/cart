<?php


namespace App\Shared\Infrastructure\Domain;


use App\Shared\Infrastructure\Common\Doctrine\Entity\AbstractEntity;
use App\Shared\Infrastructure\Common\DomainEntitySerializer;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractRepository
{
    private EntityManagerInterface $entityManager;

    private DomainEntitySerializer $serializer;

    abstract protected function getEntityClass(): string;

    public function __construct(EntityManagerInterface $entityManager, DomainEntitySerializer $serializer)
    {
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    protected function doFind(string $id): ?object
    {
        $entity = $this->entityManager->find($this->getEntityClass(), $id);
        if ($entity) {
            return $this->serializer->deserialize($entity->getData());
        }
        return null;
    }

    protected function doAdd(object $aggregate): void
    {
        $entityClass = $this->getEntityClass();
        $data = $this->serializer->serialize($aggregate);
        $this->entityManager->persist(new $entityClass($data['id']['value'], $data));
    }

    protected function doUpdate(object $aggregate): void
    {
        $data = $this->serializer->serialize($aggregate);
        /** @var AbstractEntity $entity */
        $entity = $this->entityManager->find($this->getEntityClass(), $data['id']['value']);
        $entity->setData($data);
    }
}
