<?php


namespace App\Shared\Infrastructure\Common\RequestDecoder;


use App\Shared\Infrastructure\Common\RequestDecoder\Exception\MalformedJsonException;
use Symfony\Component\HttpFoundation\Request;

final class RequestDecoder
{
    public function decode(Request $request): array
    {
        $result = json_decode($request->getContent(), true);
        if (null === $result) {
            throw new MalformedJsonException();
        }
        return $result;
    }
}
