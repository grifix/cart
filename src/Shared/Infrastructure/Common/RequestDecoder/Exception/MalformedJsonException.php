<?php


namespace App\Shared\Infrastructure\Common\RequestDecoder\Exception;


final class MalformedJsonException extends \InvalidArgumentException
{
    public function __construct()
    {
        parent::__construct('Malformed json!');
    }
}
