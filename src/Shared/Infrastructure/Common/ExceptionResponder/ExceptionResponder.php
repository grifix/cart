<?php

declare(strict_types=1);


namespace App\Shared\Infrastructure\Common\ExceptionResponder;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ExceptionResponder
{
    private int $defaultStatusCode;

    /** @var ExceptionResponseDefinition[] */
    private array $exceptionDefinitions;


    public function __construct(int $defaultStatusCode = Response::HTTP_BAD_REQUEST)
    {
        $this->defaultStatusCode = $defaultStatusCode;
    }

    public function addException(string $exceptionClass, int $code = null, $message = null): self
    {
        $this->exceptionDefinitions[$exceptionClass] = new ExceptionResponseDefinition($code, $message);
        return $this;
    }

    public function handle(\Throwable $exception): ?JsonResponse
    {
        foreach ($this->exceptionDefinitions as $exceptionClass => $definition) {
            if ($exception instanceof $exceptionClass) {
                $message = null;
                if (null !== $definition->getMessage()) {
                    $message = $definition->getMessage();
                    if (is_callable($message)) {
                        $message = $message($exception);
                    }
                }
                return new JsonResponse(
                    ['message' => $message ?? $exception->getMessage()],
                    $definition->getStatusCode() ?? $this->defaultStatusCode
                );
            }
        }
        return null;
    }
}
