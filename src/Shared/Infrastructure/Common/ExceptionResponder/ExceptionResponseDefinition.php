<?php


namespace App\Shared\Infrastructure\Common\ExceptionResponder;


final class ExceptionResponseDefinition
{
    private ?int $statusCode;

    /** @var null|string|callable */
    private $message;

    /**
     * @var string|null|callable $message
     */
    public function __construct(?int $responseCode = null, $message = null)
    {
        $this->statusCode = $responseCode;
        $this->message = $message;
    }

    public function getStatusCode(): ?int
    {
        return $this->statusCode;
    }

    /**
     * @return callable|string|null
     */
    public function getMessage()
    {
        return $this->message;
    }
}
