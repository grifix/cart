<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Common;


use Symfony\Component\PropertyAccess\Exception\AccessException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

final class ArrayAccessor
{
    private PropertyAccessor $propertyAccessor;

    private array $array;

    public function __construct(array $array)
    {
        $this->array = $array;
        $this->propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->getPropertyAccessor();
    }

    public function get(string $path, $defaultValue = null)
    {
        if (false === $this->has($path)) {
            return $defaultValue;
        }
        return $this->propertyAccessor->getValue($this->array, $this->convertPath($path));
    }

    public function has(string $path): bool
    {
        try {
            $this->propertyAccessor->getValue($this->array, $this->convertPath($path));
        } catch (AccessException $exception) {
            return false;
        }
        return true;
    }

    public function set(string $path, $value): void
    {
        $this->propertyAccessor->setValue($this->array, $this->convertPath($path), $value);
    }

    public function getArray(): array
    {
        return $this->array;
    }

    private function convertPath(string $path): string
    {
        return '[' . str_replace('.', '][', $path) . ']';
    }
}
