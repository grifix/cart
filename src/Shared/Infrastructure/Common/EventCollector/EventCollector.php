<?php


namespace App\Shared\Infrastructure\Common\EventCollector;


final class EventCollector implements EventCollectorInterface
{
    /**
     * @var []EventEnvelope[]
     */
    private array $eventCollections = [];

    public function addEvent(EventEnvelope $envelope): void
    {
        if (false === $this->hasEventCollection($envelope->getAggregateType(), $envelope->getAggregateId())) {
            $this->createEventCollection($envelope->getAggregateType(), $envelope->getAggregateId());
        }
        $this->eventCollections[$envelope->getAggregateType()] [$envelope->getAggregateId()][] = $envelope;
    }

    /**
     * @return EventEnvelope[]
     */
    public function getEvents(string $aggregateType, string $aggregateId): array
    {
        if (false === $this->hasEventCollection($aggregateType, $aggregateId)) {
            return [];
        }
        return $this->eventCollections[$aggregateType][$aggregateId];
    }

    public function clearEvents(string $aggregateType, string $aggregateId): void
    {
        if ($this->hasEventCollection($aggregateType, $aggregateId)) {
            $this->eventCollections[$aggregateType][$aggregateId] = [];
        }
    }

    private function createEventCollection(string $aggregateType, string $aggregateId): void
    {
        if (false === array_key_exists($aggregateType, $this->eventCollections)) {
            $this->eventCollections[$aggregateType] = [];
        }
        if (false === array_key_exists($aggregateId, $this->eventCollections[$aggregateType])) {
            $this->eventCollections[$aggregateType][$aggregateId] = [];
        }
    }

    private function hasEventCollection(string $aggregateType, string $aggregateId): bool
    {
        if (false === array_key_exists($aggregateType, $this->eventCollections)) {
            return false;
        }
        if (false === array_key_exists($aggregateId, $this->eventCollections[$aggregateType])) {
            return false;
        }
        return true;
    }
}
