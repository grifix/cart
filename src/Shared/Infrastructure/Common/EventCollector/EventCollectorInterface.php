<?php

namespace App\Shared\Infrastructure\Common\EventCollector;

interface EventCollectorInterface
{
    public function addEvent(EventEnvelope $envelope): void;

    /**
     * @return EventEnvelope[]
     */
    public function getEvents(string $aggregateType, string $aggregateId): array;

    public function clearEvents(string $aggregateType, string $aggregateId): void;
}
