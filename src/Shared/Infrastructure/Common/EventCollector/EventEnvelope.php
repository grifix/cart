<?php


namespace App\Shared\Infrastructure\Common\EventCollector;


final class EventEnvelope
{
    private string $aggregateId;
    private string $aggregateType;
    private object $event;

    public function __construct(string $aggregateId, string $aggregateType, object $event)
    {
        $this->aggregateId = $aggregateId;
        $this->aggregateType = $aggregateType;
        $this->event = $event;
    }

    public function getAggregateId(): string
    {
        return $this->aggregateId;
    }

    public function getAggregateType(): string
    {
        return $this->aggregateType;
    }

    public function getEvent(): object
    {
        return $this->event;
    }
}
