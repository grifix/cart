<?php


namespace App\Shared\Infrastructure\Common\EventRepository;


use App\Shared\Infrastructure\Common\JsonSerializer;
use Doctrine\DBAL\Connection;
use Ramsey\Uuid\Uuid;

final class EventRepository implements EventRepositoryInterface
{
    private const TABLE = 'outgoing_event';

    private Connection $connection;

    private JsonSerializer $serializer;

    public function __construct(Connection $connection, JsonSerializer $serializer)
    {
        $this->connection = $connection;
        $this->serializer = $serializer;
    }

    public function addEvent(object $event): void
    {
        $this->connection->insert(self::TABLE, [
            'id' => Uuid::uuid4(),
            'data' => $this->serializer->serialize($event),
            'created_at' => microtime()
        ]);
    }

    public function findEvents(array $types): \Iterator
    {


    }

    private function fetchRows(array $types, int $offset, int $limit): array
    {
        $qb = $this->connection->createQueryBuilder()
            ->select('type, data')
            ->from(self::TABLE)
            ->where('types IN :types')
            ->orderBy('created_at', 'asc')
            ->setParameter('types', $types)
            ->setFirstResult($offset)
            ->setMaxResults($limit);
        return $this->connection->fetchAllAssociative($qb->getSQL());
    }

    private function countRows(array $types):int{

    }
}
