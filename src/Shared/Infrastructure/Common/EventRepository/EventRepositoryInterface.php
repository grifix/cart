<?php


namespace App\Shared\Infrastructure\Common\EventRepository;


interface EventRepositoryInterface
{
    public function addEvent(object $event):void;

    public function findEvents(array $types):\Iterator;
}
