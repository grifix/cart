<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Migration;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200925035400 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Event store';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SCHEMA "shared"');

        $sql = <<<SQL
create table shared.event_store
(
	id uuid not null
		constraint event_store_pk
			primary key,
	type varchar not null,
	sender_type varchar not null,
	sender_id varchar not null,
	data jsonb not null,
	created_at timestamp not null,
	sent_at timestamp,
	number bigserial not null
)
SQL;
        $this->addSql($sql);

        $this->addSql('create unique index event_store_number_uindex on shared.event_store (number)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP table shared.event_store');
        $this->addSql('DROP schema shared');
    }
}
