<?php


namespace App\Shared\Infrastructure\Application;


use App\Shared\Application\QueryBusInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;

final class QueryBus implements QueryBusInterface
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function executeQuery($query)
    {
        /** @var HandledStamp $stamp */
        $stamp =  $this->messageBus->dispatch($query)->last(HandledStamp::class);
        return $stamp->getResult();
    }
}
