<?php


namespace App\Shared\Infrastructure\Application;


use App\Shared\Application\CommandBusInterface;
use App\Shared\Infrastructure\Common\Doctrine\Entity\AbstractEntity;
use App\Shared\Infrastructure\Common\EventCollector\EventCollectorInterface;
use App\Shared\Infrastructure\Common\JsonSerializer;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;

final class CommandBus implements CommandBusInterface
{
    private const EVENTS_TABLE = 'shared.event_store';

    private MessageBusInterface $commandBus;

    private MessageBusInterface $eventBus;

    private EntityManagerInterface $entityManager;

    private EventCollectorInterface $eventCollector;

    private JsonSerializer $serializer;

    private Connection $connection;

    private array $eventsToPublish = [];

    public function __construct(
        MessageBusInterface $messageBus,
        MessageBusInterface $eventBus,
        EntityManagerInterface $entityManager,
        EventCollectorInterface $eventCollector,
        JsonSerializer $serializer
    ) {
        $this->commandBus = $messageBus;
        $this->eventBus = $eventBus;
        $this->entityManager = $entityManager;
        $this->eventCollector = $eventCollector;
        $this->serializer = $serializer;
        $this->connection = $entityManager->getConnection();
    }

    public function executeCommand($command): void
    {
        try{
            $this->commandBus->dispatch($command);
        }
        catch (HandlerFailedException $exception){
            throw $exception->getPrevious();
        }
        finally {
            $this->entityManager->beginTransaction();
            $this->entityManager->flush();
            $this->saveEvents();
            $this->entityManager->commit();

            $this->publishEvents();
        }
    }

    private function publishEvents(): void
    {
        foreach ($this->eventsToPublish as $id => $event) {
            $this->connection->beginTransaction();
            $this->eventBus->dispatch($event);
            $this->connection->update(
                self::EVENTS_TABLE,
                [
                    'sent_at' => $this->getTime()
                ],
                [
                    'id' => $id
                ]
            );
            unset($this->eventsToPublish[$id]);
            $this->connection->commit();
        }
    }

    private function saveEvents(): void
    {
        foreach ($this->entityManager->getUnitOfWork()->getIdentityMap() as $entities) {
            /** @var AbstractEntity $entity */
            foreach ($entities as $entity) {
                $aggregateType = $entity->getData()['__type'];
                $envelopes = $this->eventCollector->getEvents($aggregateType, $entity->getId());
                foreach ($envelopes as $envelope) {
                    $eventId = (string)Uuid::uuid4();
                    $this->connection->insert(
                        self::EVENTS_TABLE,
                        [
                            'id' => $eventId,
                            'type' => get_class($envelope->getEvent()),
                            'sender_type' => $envelope->getAggregateType(),
                            'sender_id' => $envelope->getAggregateId(),
                            'data' => $this->serializer->serialize($envelope->getEvent()),
                            'created_at' => $this->getTime()
                        ]
                    );
                    $this->eventsToPublish[$eventId] = $envelope->getEvent();
                }
                $this->eventCollector->clearEvents($aggregateType, $entity->getId());
            }
        }
    }

    private function getTime(): string
    {
        return (new \DateTime("now", new \DateTimeZone("UTC")))->format('Y-m-d H:i:s.u');
    }
}
