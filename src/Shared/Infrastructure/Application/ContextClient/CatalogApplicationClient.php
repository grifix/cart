<?php

namespace App\Shared\Infrastructure\Application\ContextClient;

use App\Catalog\Application\Command\Product\CreateProduct\CreateProductCommand;
use App\Catalog\Application\Projection\ProductProjection\ProductFilter;
use App\Catalog\Application\Query\FindProducts\FindProductsQuery;
use App\Catalog\Application\Query\FindProducts\FindProductsQueryResult;
use App\Shared\Application\CommandBusInterface;
use App\Shared\Application\ContextClient\CatalogService\CatalogClientInterface;
use App\Shared\Application\ContextClient\CatalogService\Dto\CreateProductDto;
use App\Shared\Application\ContextClient\CatalogService\Dto\ProductDto;
use App\Shared\Application\ContextClient\CatalogService\Exception\ProductNotFoundException;
use App\Shared\Application\Query\GetUuid\GetUuidQuery;
use App\Shared\Application\Query\GetUuid\GetUuidQueryResult;
use App\Shared\Application\QueryBusInterface;

final class CatalogApplicationClient implements CatalogClientInterface
{
    private QueryBusInterface $queryBus;

    private CommandBusInterface $commandBus;

    public function __construct(
        QueryBusInterface $queryBus,
        CommandBusInterface $commandBus
    )
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
    }

    public function findProduct(string $productId): ProductDto
    {
        /** @var FindProductsQueryResult $queryResult */
        $queryResult = $this->queryBus->executeQuery(new FindProductsQuery(ProductFilter::create()->setProductId($productId)));
        if (count($queryResult->getProducts()) < 1) {
            throw new ProductNotFoundException($productId);
        }
        $product = $queryResult->getProducts()[0];
        return new ProductDto($product->getId(),
            $product->getName(), $product->getPrice());
    }

    public function createProduct(CreateProductDto $createProduct): string
    {
        $id = $this->getId();
        $this->commandBus->executeCommand(new CreateProductCommand(
            $id,
            $createProduct->getName(),
            $createProduct->getPrice()
        ));
        return $id;
    }

    public function productNameExists(string $name): bool
    {
        $queryResult = $this->queryBus->executeQuery(new FindProductsQuery(ProductFilter::create()->setName($name)));
        if (count($queryResult->getProducts()) > 0) {
            return true;
        }
        return false;
    }

    private function getId(): string
    {
        /** @var GetUuidQueryResult $result */
        $result = $this->queryBus->executeQuery(new GetUuidQuery());
        return $result->getUuid();
    }
}
