<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Application\Dictionary;

use App\Shared\Application\Dictionary\CurrencyDictionaryInterface;

final class CurrencyDictionary implements CurrencyDictionaryInterface
{
    /** @var string[] */
    private array $currencies;

    /**
     * CurrencyDictionary constructor.
     * @param string[] $currencies
     */
    public function __construct(array $currencies)
    {
        $this->currencies = $currencies;
    }

    public function getCurrencies(): array
    {
        return $this->currencies;
    }
}
