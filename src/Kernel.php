<?php

namespace App;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    private array $contexts = [
        'shared',
        'catalog',
        'cart'
    ];

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $container->import('../config/services.yaml');
        $container->import('../config/{packages}/*.yaml');
        $container->import('../config/{packages}/' . $this->environment . '/*.yaml');
        foreach ($this->contexts as $context) {
            $path = sprintf('%s/config/contexts/%s/services.yaml',$this->getProjectDir(), $context);
            if(file_exists($path)){
                $container->import($path);
            }
            $path = sprintf('%s/config/contexts/%s/%s/services.yaml',$this->getProjectDir(), $context, $this->environment);
            if(file_exists($path)){
                $container->import($path);
            }
        }
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import('../config/{routes}/' . $this->environment . '/*.yaml');
        $routes->import('../config/{routes}/*.yaml');

        foreach ($this->contexts as $context) {
            $path = sprintf('%s/config/contexts/%s/routes.yaml',$this->getProjectDir(), $context);
            if(file_exists($path)){
                $routes->import($path);
            }
            $path = sprintf('%s/config/contexts/%s/%s/routes.yaml',$this->getProjectDir(), $context, $this->environment);
            if(file_exists($path)){
                $routes->import($path);
            }
        }
    }
}
