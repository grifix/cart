<?php


namespace App\Tests\Shared\Unit\Uuid;


use App\Shared\Domain\Uuid\Exceptions\InvalidUuidException;
use App\Shared\Domain\Uuid\Uuid;
use App\Tests\Shared\Unit\Uuid\Builders\UuidObjectBuilder;
use App\Tests\Shared\Unit\Uuid\Builders\UuidOutsideMockBuilder;
use PHPUnit\Framework\TestCase;

final class UuidTest extends TestCase
{
    public function testItCreates(): void
    {
        $expectedUuid = 'a4f77ae9-5a65-4af1-8252-211078ca91c4';
        $uuid = new Uuid(UuidOutsideMockBuilder::create()->build(), $expectedUuid);
        self::assertEquals($expectedUuid, (string)$uuid);
    }

    public function testItDoesNotCreatesWithWrongUuid(): void
    {
        $this->expectException(InvalidUuidException::class);
        $this->expectExceptionMessage('Uuid "invalid" is invalid!');
        UuidObjectBuilder::create()
            ->setValue('invalid')
            ->setOutside(UuidOutsideMockBuilder::create()->setIsValidUuidResult(false)->build())
            ->build();
    }

    /**
     * @dataProvider itEqualsDataProvider
     */
    public function testItEquals(string $uuidA, string $uuidB, bool $expectedResult): void
    {
        $uuidA = UuidObjectBuilder::buildWithValue($uuidA);
        $uuidB = UuidObjectBuilder::buildWithValue($uuidB);
        self::assertEquals($expectedResult, $uuidA->isEqualTo($uuidB));
    }

    public function itEqualsDataProvider(): array
    {
        return [
            ['57a10a11-e79f-4b81-bac5-25e043b00bcf', '57a10a11-e79f-4b81-bac5-25e043b00bcf', true],
            ['57a10a11-e79f-4b81-bac5-25e043b00bcf', '663df2b6-236a-4061-8066-1612df6eceee', false]
        ];
    }
}
