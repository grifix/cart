<?php
declare(strict_types=1);

namespace App\Tests\Shared\Unit\Uuid\Builders;

use App\Shared\Domain\Uuid\Uuid;
use App\Shared\Domain\Uuid\UuidOutsideInterface;
use Ramsey\Uuid\Uuid as RamseyUuid;

final class UuidObjectBuilder
{
    private string $value = '1172df03-1cb4-4ee2-b082-b4ffe510e6f3';

    private UuidOutsideInterface $outside;

    private function __construct()
    {
        $this->outside = UuidOutsideMockBuilder::create()->build();
    }

    public static function create(): self
    {
        return new self();
    }

    public function setValue(string $value): UuidObjectBuilder
    {
        $this->value = $value;
        return $this;
    }

    public function setOutside(UuidOutsideInterface $outside): UuidObjectBuilder
    {
        $this->outside = $outside;
        return $this;
    }

    public function build(): Uuid
    {
        return new Uuid($this->outside, $this->value);
    }

    public static function buildWithValue(string $value): Uuid
    {
        return self::create()->setValue($value)->build();
    }

    public static function buildRandom(): Uuid
    {
        return self::create()->setValue((string)RamseyUuid::uuid4())->build();
    }
}
