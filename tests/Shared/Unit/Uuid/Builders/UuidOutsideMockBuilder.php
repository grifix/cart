<?php
declare(strict_types=1);

namespace App\Tests\Shared\Unit\Uuid\Builders;


use App\Shared\Domain\Uuid\UuidOutsideInterface;
use Mockery\Mock;

final class UuidOutsideMockBuilder
{
    private bool $isValidUuidResult = true;

    private function __construct()
    {

    }

    public static function create(): self
    {
        return new self();
    }

    public function setIsValidUuidResult(bool $isValidUuidResult): self
    {
        $this->isValidUuidResult = $isValidUuidResult;
        return $this;
    }

    public function build(): UuidOutsideInterface|Mock
    {
        $result = \Mockery::mock(UuidOutsideInterface::class);

        $result->shouldReceive('isValidUuid')->byDefault()->andReturn($this->isValidUuidResult);

        return $result;
    }
}
