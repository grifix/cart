<?php

declare(strict_types=1);

namespace App\Tests\Shared\Unit;


use App\Shared\Infrastructure\Common\ArrayAccessor;
use PHPUnit\Framework\TestCase;

final class ArrayAccessorTest extends TestCase
{
    public function testLifecycle(): void
    {
        $arrayAccessor = new ArrayAccessor([]);
        $arrayAccessor->set('name', 'Notebook');
        self::assertEquals('Notebook', $arrayAccessor->get('name'));
        self::assertFalse($arrayAccessor->has('price.currency'));
        self::assertEquals('USD', $arrayAccessor->get('price.currency', 'USD'));
        self::assertFalse($arrayAccessor->has('price.currency'));
        $arrayAccessor->set('price.currency', 'PLN');
        self::assertEquals('PLN', $arrayAccessor->get('price.currency'));
    }
}
