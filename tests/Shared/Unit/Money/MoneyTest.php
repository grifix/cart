<?php
declare(strict_types=1);

namespace App\Tests\Shared\Unit\Money;


use App\Shared\Domain\Money\Currency\Currency;
use App\Shared\Domain\Money\Exceptions\AmountMustBePositiveException;
use App\Shared\Domain\Money\Money;
use App\Tests\Shared\Unit\Money\Builders\MoneyObjectBuilder;
use App\Tests\Shared\Unit\Money\Currency\Builders\CurrencyObjectBuilder;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

final class MoneyTest extends TestCase
{
    /**
     * @dataProvider validAmountProvider
     */
    public function testItCreates(string|int $amount): void
    {
        $money = new Money(
            $amount,
            CurrencyObjectBuilder::create()->setCode('USD')->build()
        );
        Assert::assertEquals(sprintf('%s USD', (string)$amount), (string)$money);
    }

    public function validAmountProvider(): array
    {
        return [
            [100],
            [0],
            ['0'],
            ['100'],
            ['99999999999']
        ];
    }

    /**
     * @dataProvider nonPositiveAmountProvider
     */
    public function testItDoesNotCreatesWithInvalidAmount(int|string $amount): void
    {
        $this->expectException(AmountMustBePositiveException::class);
        MoneyObjectBuilder::buildWithAmount($amount);
    }

    public function nonPositiveAmountProvider(): array
    {
        return [
            [-100],
            ['-100']
        ];
    }

    public function testItCreatesWithAmount(): void
    {
        $money = MoneyObjectBuilder::buildWithCurrency(Currency::CODE_USD);
        $newMoney = $money->withAmount(200);
        Assert::assertEquals('200 USD', (string)$newMoney);
    }

    public function testItAdds(): void
    {
        $moneyA = MoneyObjectBuilder::buildWithAmountAndCurrency(100, 'USD');
        $moneyB = MoneyObjectBuilder::buildWithAmountAndCurrency(20, 'USD');
        $expectedResult = MoneyObjectBuilder::buildWithAmountAndCurrency(120, 'USD');

        self::assertTrue($moneyA->add($moneyB)->isEqualTo($expectedResult));
    }

    public function testItSubtracts(): void
    {
        $moneyA = MoneyObjectBuilder::buildWithAmountAndCurrency(120, 'USD');
        $moneyB = MoneyObjectBuilder::buildWithAmountAndCurrency(20, 'USD');
        $expectedResult = MoneyObjectBuilder::buildWithAmountAndCurrency(100, 'USD');

        self::assertTrue($moneyA->subtract($moneyB)->isEqualTo($expectedResult));
    }

    public function testItDoesNotSubtractToNegative(): void
    {
        $moneyA = MoneyObjectBuilder::buildWithAmountAndCurrency(20, 'USD');
        $moneyB = MoneyObjectBuilder::buildWithAmountAndCurrency(100, 'USD');


        $this->expectException(AmountMustBePositiveException::class);
        $moneyA->subtract($moneyB);
    }
}
