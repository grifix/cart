<?php


namespace App\Tests\Shared\Unit\Money\Currency;


use App\Shared\Domain\Money\Currency\Currency;
use App\Shared\Domain\Money\Currency\Exceptions\CurrencyIsNotSupportedException;
use App\Tests\Shared\Unit\Money\Currency\Builders\CurrencyObjectBuilder;
use App\Tests\Shared\Unit\Money\Currency\Builders\CurrencyOutsideMockBuilder;
use PHPUnit\Framework\TestCase;

final class CurrencyTest extends TestCase
{

    public function testItCreates(): void
    {
        $currency = new Currency(
            CurrencyOutsideMockBuilder::create()->build(),
            Currency::CODE_USD
        );

        self::assertEquals(Currency::CODE_USD, (string)$currency);
    }

    public function testDoesNotCreatesWithNotSupportedCurrency(): void
    {
        $this->expectException(CurrencyIsNotSupportedException::class);
        CurrencyObjectBuilder::create()
            ->setOutside(
                CurrencyOutsideMockBuilder::create()->setIsCurrencySupportedResult(false)->build()
            )->build();
    }
}
