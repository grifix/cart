<?php
declare(strict_types=1);

namespace App\Tests\Shared\Unit\Money\Currency\Builders;


use App\Shared\Domain\Money\Currency\Currency;
use App\Shared\Domain\Money\Currency\CurrencyOutsideInterface;

final class CurrencyObjectBuilder
{
    private string $code = 'USD';

    private CurrencyOutsideInterface $outside;

    private function __construct()
    {
        $this->outside = CurrencyOutsideMockBuilder::create()->build();
    }

    public static function create(): self
    {
        return new self();
    }

    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    public function setOutside($outside): self
    {
        $this->outside = $outside;
        return $this;
    }

    public function build(): Currency
    {
        return new Currency($this->outside, $this->code);
    }

    public static function buildWithCode(string $currencyCode): Currency
    {
        return self::create()->setCode($currencyCode)->build();
    }
}
