<?php
declare(strict_types=1);

namespace App\Tests\Shared\Unit\Money\Currency\Builders;


use App\Shared\Domain\Money\Currency\CurrencyOutsideInterface;
use Mockery;
use Mockery\MockInterface;

final class CurrencyOutsideMockBuilder
{
    private bool $isCurrencySupportedResult = true;

    private function __construct()
    {
    }

    public function setIsCurrencySupportedResult(bool $isCurrencySupportedResult): self
    {
        $this->isCurrencySupportedResult = $isCurrencySupportedResult;
        return $this;
    }

    public static function create(): self
    {
        return new self();
    }

    public function build(): CurrencyOutsideInterface|MockInterface
    {
        $result = Mockery::mock(CurrencyOutsideInterface::class);

        $result->shouldReceive('isCurrencySupported')->andReturn($this->isCurrencySupportedResult);

        return $result;
    }


}
