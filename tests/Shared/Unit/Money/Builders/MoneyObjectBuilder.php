<?php
declare(strict_types=1);

namespace App\Tests\Shared\Unit\Money\Builders;


use App\Shared\Domain\Money\Currency\Currency;
use App\Shared\Domain\Money\Money;
use App\Tests\Shared\Unit\Money\Currency\Builders\CurrencyObjectBuilder;

final class MoneyObjectBuilder
{
    private string|int $amount = 100;

    private Currency $currency;

    private function __construct()
    {
        $this->currency = CurrencyObjectBuilder::create()->build();
    }

    public static function create(): self
    {
        return new self();
    }

    public function setAmount(int|string $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;
        return $this;
    }

    public function build(): Money
    {
        return new Money($this->amount, $this->currency);
    }

    public static function buildWithAmountAndCurrency(string|int $amount, string $currencyCode = null): Money
    {
        $builder = self::create();
        $builder->setAmount($amount);
        $builder->setCurrency(CurrencyObjectBuilder::buildWithCode($currencyCode));
        return $builder->build();
    }

    public static function buildWithAmount(string|int $amount): Money
    {
        $builder = self::create();
        $builder->setAmount($amount);
        return $builder->build();
    }

    public static function buildWithCurrency(string $currencyCode): Money
    {
        $builder = self::create();
        $builder->setCurrency(CurrencyObjectBuilder::buildWithCode($currencyCode));
        return $builder->build();
    }
}
