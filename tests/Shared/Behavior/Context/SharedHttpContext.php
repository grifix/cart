<?php


namespace App\Tests\Shared\Behavior\Context;


use App\Tests\Shared\Utils\HttpClient\HttpClient;
use Behat\Behat\Context\Context;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;

final class SharedHttpContext implements Context
{
    private HttpClient $httpClient;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @When /^I check application health$/
     */
    public function iCheckApplicationHealth()
    {
        $this->httpClient->get('shared_health_check');
    }

    /**
     * @Then /^Application health should be ok$/
     */
    public function applicationHealthShouldBeOk()
    {
        $this->httpClient->assertLastResponseCode(Response::HTTP_OK);
        $this->httpClient->assertLastResponseBody(['status' => 'ok']);
    }
}
