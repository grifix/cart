<?php


namespace App\Tests\Shared\Behavior\Context;


use App\Shared\Application\Dictionary\CurrencyDictionaryInterface;
use App\Tests\Shared\Mock\CurrencyDictionaryMock;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Doctrine\DBAL\Connection;
use Symfony\Component\Yaml\Yaml;

final class SharedSetUpContext implements Context
{
    private string $rootDir;

    private string $dumpPath;

    private array $connectionParams;

    private CurrencyDictionaryMock $currencyDictionary;

    public function __construct(
        string $rootDir,
        Connection $connection,
        CurrencyDictionaryInterface $currencyDictionary
    )
    {
        $this->rootDir = $rootDir;
        $this->dumpPath = $rootDir . '/var/test_dump.sql';
        $this->connectionParams = $connection->getParams();

        /** @var CurrencyDictionaryMock $currencyDictionary */
        $this->currencyDictionary = $currencyDictionary;
    }

    /**
     * @BeforeScenario
     */
    public function beforeScenario(BeforeScenarioScope $scope): void
    {
        if (!is_file($this->dumpPath)) {
            $this->exec($this->rootDir . '/bin/console doctrine:database:drop --env=test --no-interaction --if-exists --force');
            $this->exec($this->rootDir . '/bin/console doctrine:database:create --env=test --no-interaction');
            $this->exec($this->rootDir . '/bin/console doctrine:migrations:migrate --env=test --no-interaction');
            $this->exec(sprintf(
                'export PGPASSWORD="%s" && pg_dump -h %s -p %s -d %s -U %s -wc --if-exists > %s',
                $this->connectionParams['password'],
                $this->connectionParams['host'],
                $this->connectionParams['port'],
                $this->connectionParams['dbname'],
                $this->connectionParams['user'],
                $this->dumpPath
            ));
        }
        $this->exec(sprintf(
            'export PGPASSWORD="%s" && psql -h %s -p %s -d %s -U %s -w < %s',
            $this->connectionParams['password'],
            $this->connectionParams['host'],
            $this->connectionParams['port'],
            $this->connectionParams['dbname'],
            $this->connectionParams['user'],
            $this->dumpPath
        ));
    }

    /**
     * @Given /^supported currencies are:$/
     */
    public function supportedCurrenciesAre(string $currencies)
    {
        $this->currencyDictionary->setCurrencies(Yaml::parse($currencies));
    }

    private function exec(string $command)
    {
        $output = [];
        $result = 0;
        exec($command, $output, $result);
        if (0 !== $result) {
            throw new \Exception(implode("\n", $output));
        }
    }
}
