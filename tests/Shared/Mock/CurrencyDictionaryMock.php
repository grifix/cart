<?php

declare(strict_types=1);

namespace App\Tests\Shared\Mock;


use App\Shared\Application\Dictionary\CurrencyDictionaryInterface;

final class CurrencyDictionaryMock implements CurrencyDictionaryInterface
{

    /** @var string[] */
    private array $currencies;

    /**
     * @param string[] $currencies
     */
    public function __construct(array $currencies)
    {
        $this->currencies = $currencies;
    }

    public function getCurrencies(): array
    {
        return $this->currencies;
    }

    /**
     * @param string[] $currencies
     */
    public function setCurrencies(array $currencies): void
    {
        $this->currencies = $currencies;
    }
}
