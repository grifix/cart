<?php


namespace App\Tests\Shared\Utils;


use App\Shared\Application\CommandBusInterface;
use Ramsey\Uuid\Uuid;

abstract class AbstractFixtureMother
{
    protected CommandBusInterface $commandBus;

    public function __construct(CommandBusInterface $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    protected function createId(): string
    {
        return (string)Uuid::uuid4();
    }
}
