<?php


namespace App\Tests\Shared\Utils;


/**
 * @template T
 */
final class FixtureRegistry
{
    /** @var object[][] */
    private array $fixtures = [];

    public function addFixture(object $fixture, string $alias)
    {
        $type = get_class($fixture);
        if (!array_key_exists($type, $this->fixtures)) {
            $this->fixtures[$type] = [];
        }
        if (array_key_exists($alias, $this->fixtures[$type])) {
            throw new \Exception('Fixture %s with alias %s already exists!', $type, $alias);
        }
        $this->fixtures[$type][$alias] = $fixture;
    }

    /**
     * @template T
     * @param T $type
     * @return T
     */
    public function getFixture(string $type, string $alias): object
    {
        if (!array_key_exists($type, $this->fixtures) || !array_key_exists($alias, $this->fixtures[$type])) {
            throw new \Exception(sprintf('Fixture %s with alias %s does not exist!', $type, $alias));
        }
        return $this->fixtures[$type][$alias];
    }
}
