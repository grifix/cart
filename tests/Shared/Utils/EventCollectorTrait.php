<?php


namespace App\Tests\Shared\Utils;


use Mockery\MockInterface;

trait EventCollectorTrait
{
    protected array $publishedEvents = [];

    public function clearPublishedEvents(): void
    {
        $this->publishedEvents = [];
    }

    public function getPublishedEvents(): array
    {
        return $this->publishedEvents;
    }

    public function addPublishedEvent(object $event): void
    {
        $this->publishedEvents[] = $event;
    }

    protected function enableEventCollector(MockInterface $mock): void
    {
        $mock->shouldReceive('publishEvent')->byDefault()->andReturnUsing(function (object $event) {
            $this->publishedEvents[] = $event;
        });
    }
}
