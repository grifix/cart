<?php


namespace App\Tests\Shared\Utils\HttpClient;


use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\RouterInterface;

final class HttpClient
{
    private KernelInterface $kernel;

    private RouterInterface $router;

    private Response $lastResponse;

    public function __construct(KernelInterface $kernel, RouterInterface $router)
    {
        $this->kernel = $kernel;
        $this->router = $router;
    }

    private function sendRequest(
        string $route,
        string $method,
        array $payload = [],
        array $parameters = [],
        array $query = []
    ): void {
        $this->lastResponse = $this->kernel->handle(
            Request::create(
                $this->router->generate($route, $parameters),
                $method,
                $query,
                [],
                [],
                [
                    'HTTP_ACCEPT' => 'application/json',
                    'CONTENT_TYPE' => 'application/json',
                ],
                json_encode($payload)
            )
        );
        if ($this->lastResponse->getStatusCode() === Response::HTTP_INTERNAL_SERVER_ERROR) {
            throw new \Exception($this->getLastResponse()->getContent());
        }
    }

    public function get(string $route, array $parameters = [], array $query = []): void
    {
        $this->sendRequest($route, 'GET', [], $parameters, $query);
    }

    public function post(string $route, array $parameters = [], array $payload = [], array $query = [])
    {
        $this->sendRequest($route, 'POST', $payload, $parameters, $query);
    }

    public function delete(string $route, array $parameters = [], array $payload = [], array $query = [])
    {
        $this->sendRequest($route, 'DELETE', $payload, $parameters, $query);
    }

    public function patch(string $route, array $parameters = [], array $payload = [], array $query = [])
    {
        $this->sendRequest($route, 'PATH', $payload, $parameters, $query);
    }

    public function put(string $route, array $parameters = [], array $payload = [], array $query = [])
    {
        $this->sendRequest($route, 'PUT', $payload, $parameters, $query);
    }

    public function getLastResponse(): Response
    {
        return $this->lastResponse;
    }

    public function assertLastResponseCode(int $expectedCode): void
    {
        Assert::assertEquals($expectedCode, $this->lastResponse->getStatusCode());
    }

    public function assertLastResponseBody(array $expectedBody): void
    {
        Assert::assertEquals($expectedBody, json_decode($this->lastResponse->getContent(), true));
    }

    public function getLastResponseArray(): array
    {
        return json_decode($this->lastResponse->getContent(), true);
    }

    public function assertLastError(int $code, string $message): void
    {
        $this->assertLastResponseCode($code);
        Assert::assertEquals($message, $this->getLastResponseArray()['message']);
    }

    public function assertLastErrorResponse(int $code, array $response): void
    {
        $this->assertLastResponseCode($code);
        Assert::assertEquals($this->getLastResponseArray(), $response);
    }
}
