<?php


namespace App\Tests\Cart\Unit\Cart;


use App\Cart\Domain\Cart\Cart;
use App\Cart\Domain\Cart\Events\CartCreatedEvent;
use App\Cart\Domain\Cart\Events\ProductAddedToCartEvent;
use App\Cart\Domain\Cart\Events\ProductRemovedFromCartEvent;
use App\Cart\Domain\Cart\Exceptions\CartItemsLimitExceedException;
use App\Cart\Domain\Cart\Exceptions\ProductAlreadyAddedToCartException;
use App\Cart\Domain\Cart\Exceptions\ProductNotExistInCartException;
use App\Tests\Cart\Unit\Cart\Builders\CartObjectBuilder;
use App\Tests\Cart\Unit\Cart\Builders\CartOutsideMockBuilder;
use App\Tests\Shared\Unit\Money\Builders\MoneyObjectBuilder;
use App\Tests\Shared\Unit\Uuid\Builders\UuidObjectBuilder;
use PHPUnit\Framework\TestCase;

final class CartTest extends TestCase
{

    public function testItCrates(): void
    {
        $outsideBuilder = CartOutsideMockBuilder::create();
        new Cart(
            $outsideBuilder->build(),
            UuidObjectBuilder::buildWithValue('96571523-126b-4e42-878a-8f6dde26bb19')
        );
        $events = $outsideBuilder->getPublishedEvents();
        self::assertCount(1, $events);
        self::assertEquals(
            new CartCreatedEvent(
                UuidObjectBuilder::buildWithValue('96571523-126b-4e42-878a-8f6dde26bb19'),
                MoneyObjectBuilder::buildWithAmount(0)
            ),
            $events[0]
        );
    }

    public function testItAddsAndRemovesProduct(): void
    {
        $outsideBuilder = CartOutsideMockBuilder::create()
            ->setGetProductPriceResult(MoneyObjectBuilder::buildWithAmount(5));

        $cart = CartObjectBuilder::create()
            ->setOutside($outsideBuilder->build())
            ->setId(UuidObjectBuilder::buildWithValue('96571523-126b-4e42-878a-8f6dde26bb19'))
            ->build();
        $outsideBuilder->clearPublishedEvents();

        $cart->addProduct(UuidObjectBuilder::buildWithValue('a4b36d6f-3443-4f23-97d3-fc534a2fd34b'));

        $events = $outsideBuilder->getPublishedEvents();
        self::assertCount(1, $events);
        self::assertEquals(
            new ProductAddedToCartEvent(
                UUidObjectBuilder::buildWithValue('96571523-126b-4e42-878a-8f6dde26bb19'),
                UuidObjectBuilder::buildWithValue('a4b36d6f-3443-4f23-97d3-fc534a2fd34b'),
                MoneyObjectBuilder::buildWithAmount(5),
                MoneyObjectBuilder::buildWithAmount(5)
            ),
            $events[0]
        );
        $outsideBuilder->clearPublishedEvents();

        $cart->removeProduct(UuidObjectBuilder::buildWithValue('a4b36d6f-3443-4f23-97d3-fc534a2fd34b'));
        $events = $outsideBuilder->getPublishedEvents();
        self::assertCount(1, $events);
        self::assertEquals(
            new ProductRemovedFromCartEvent(
                UUidObjectBuilder::buildWithValue('96571523-126b-4e42-878a-8f6dde26bb19'),
                UuidObjectBuilder::buildWithValue('a4b36d6f-3443-4f23-97d3-fc534a2fd34b'),
                MoneyObjectBuilder::buildWithAmount(0),
            ),
            $events[0]
        );
    }


    public function testItDoesNotAddIfLimitExceed(): void
    {
        $cart = CartObjectBuilder::create()
            ->setOutside(CartOutsideMockBuilder::create()->setGetItemsLimitResult(3)->build())
            ->build();

        $cart->addProduct(UuidObjectBuilder::buildRandom());
        $cart->addProduct(UuidObjectBuilder::buildRandom());
        $cart->addProduct(UuidObjectBuilder::buildRandom());
        $this->expectException(CartItemsLimitExceedException::class);
        $cart->addProduct(UuidObjectBuilder::buildRandom());
    }

    public function testItDoesNotAddExistedProduct(): void
    {
        $cart = CartObjectBuilder::buildDefault();
        $cart->addProduct(UuidObjectBuilder::buildWithValue('698dcb70-55e6-4054-a82c-70a7e2a11306'));
        $this->expectException(ProductAlreadyAddedToCartException::class);
        $cart->addProduct(UuidObjectBuilder::buildWithValue('698dcb70-55e6-4054-a82c-70a7e2a11306'));
    }

    public function testIdDoesNotRemoveNotExistedProduct(): void
    {
        $cart = CartObjectBuilder::buildDefault();
        $this->expectException(ProductNotExistInCartException::class);
        $cart->removeProduct(UuidObjectBuilder::buildRandom());
    }
}
