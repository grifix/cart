<?php
declare(strict_types=1);

namespace App\Tests\Cart\Unit\Cart\Builders;


use App\Cart\Domain\Cart\CartOutsideInterface;
use App\Shared\Domain\Money\Money;
use App\Tests\Shared\Unit\Money\Builders\MoneyObjectBuilder;
use App\Tests\Shared\Utils\EventCollectorTrait;
use Mockery\MockInterface;

final class CartOutsideMockBuilder
{
    use EventCollectorTrait;

    private int $getItemsLimitResult = 3;

    private Money $getProductPriceResult;

    private Money $createMoneyResult;

    private function __construct()
    {
        $this->createMoneyResult = MoneyObjectBuilder::create()->setAmount(0)->build();
        $this->getProductPriceResult = MoneyObjectBuilder::create()->setAmount(100)->build();
    }

    public static function create(): self
    {
        return new self();
    }

    public function setGetItemsLimitResult(int $getItemsLimitResult): self
    {
        $this->getItemsLimitResult = $getItemsLimitResult;
        return $this;
    }

    public function setGetProductPriceResult(Money $getProductPriceResult): self
    {
        $this->getProductPriceResult = $getProductPriceResult;
        return $this;
    }

    public function build(): CartOutsideInterface|MockInterface
    {
        $result = \Mockery::mock(CartOutsideInterface::class);

        $this->enableEventCollector($result);

        $result->shouldReceive('getItemsLimit')->byDefault()->andReturn($this->getItemsLimitResult);
        $result->shouldReceive('getProductPrice')->byDefault()->andReturn($this->getProductPriceResult);
        $result->shouldReceive('createMoney')->byDefault()->andReturn($this->createMoneyResult);

        return $result;
    }


}
