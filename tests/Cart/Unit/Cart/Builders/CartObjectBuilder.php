<?php
declare(strict_types=1);

namespace App\Tests\Cart\Unit\Cart\Builders;


use App\Cart\Domain\Cart\Cart;
use App\Cart\Domain\Cart\CartOutsideInterface;
use App\Shared\Domain\Uuid\Uuid;
use App\Tests\Shared\Unit\Uuid\Builders\UuidObjectBuilder;

final class CartObjectBuilder
{

    private CartOutsideInterface $outside;

    private Uuid $id;

    private function __construct()
    {
        $this->outside = CartOutsideMockBuilder::create()->build();
        $this->id = UuidObjectBuilder::create()->build();
    }

    public static function create(): self
    {
        return new self();
    }

    public function setOutside(CartOutsideInterface $outside): self
    {
        $this->outside = $outside;
        return $this;
    }

    public function setId(Uuid $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function build(): Cart
    {
        return new Cart($this->outside, $this->id);
    }

    public static function buildDefault(): Cart
    {
        return self::create()->build();
    }
}
