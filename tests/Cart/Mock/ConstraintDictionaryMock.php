<?php

declare(strict_types=1);

namespace App\Tests\Cart\Mock;


use App\Cart\Application\Dictionary\ConstraintsDictionary\ConstraintsDictionaryInterface;

final class ConstraintDictionaryMock implements ConstraintsDictionaryInterface
{
    private int $maxCartItemsLimit;

    public function __construct(int $maxCartItemsLimit)
    {
        $this->maxCartItemsLimit = $maxCartItemsLimit;
    }

    public function getMaxCartItemsLimit(): int
    {
        return $this->maxCartItemsLimit;
    }

    public function setMaxCartItemsLimit(int $maxCartItemsLimit): ConstraintDictionaryMock
    {
        $this->maxCartItemsLimit = $maxCartItemsLimit;
        return $this;
    }

}
