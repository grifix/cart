Feature: Add product to cart
  Background:
    Given there is a cart "My cart"
    And there are products:
      | name                          | price | currency |
      | The Godfather                 | 59.99 | PLN      |
      | Steve Jobs                    | 49.95 | PLN      |
      | The Return of Sherlock Holmes | 39.99 | PLN      |
    And the cart "My cart" has products:
    """
    - The Godfather
    - Steve Jobs
    - The Return of Sherlock Holmes
    """

  Scenario: Remove product from the cart:
    When I remove product "The Godfather" from the cart "My cart"
    Then cart "My cart" should have products:
      | name                          | price | currency |
      | Steve Jobs                    | 49.95 | PLN      |
      | The Return of Sherlock Holmes | 39.99 | PLN      |
    And cart "My cart" total should be "89.94"

  Scenario: Remove not existing product from the cart:
    When I remove product with id "94e92570-5b62-420a-b22f-a149020cce17" from the cart "My cart"
    Then I should get an error that product with id "94e92570-5b62-420a-b22f-a149020cce17" does not exist in the cart "My cart"

  Scenario: Remove product form non existing cart:
    When I remove product "The Godfather" from cart with id "957fd3f3-9e24-42a3-8e7d-834e4ded71d0"
    Then I should gat an error that cart "957fd3f3-9e24-42a3-8e7d-834e4ded71d0" does not exist
