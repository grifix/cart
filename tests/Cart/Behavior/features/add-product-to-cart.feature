Feature: Add product to cart
  Background:
    And there is a cart "My cart"
    And there are products:
      | name                          | price | currency |
      | The Godfather                 | 59.99 | PLN      |
      | Steve Jobs                    | 49.95 | PLN      |

  Scenario: Add product to cart
    When I add product "The Godfather" into the cart "My cart"
    And I add product "Steve Jobs" into the cart "My cart"
    Then cart "My cart" should have products:
      | name                          | price | currency |
      | The Godfather                 | 59.99 | PLN      |
      | Steve Jobs                    | 49.95 | PLN      |
    And cart "My cart" total should be "109.94"

  Scenario: Try to add too much product to cart
    Given the cart items limit is "3"
    And the cart "My cart" has 3 items
    When I add product to the cart "My cart"
    Then I should get an error that i cannot add to cart "My cart" more than "3" items

  Scenario: Add product to cart twice
    Given the cart "My cart" has product "The Godfather"
    When I add product "The Godfather" into the cart "My cart"
    Then I should get and error that product "The Godfather" is already added to cart "My cart"

