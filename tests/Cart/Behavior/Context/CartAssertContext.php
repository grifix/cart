<?php
declare(strict_types=1);

namespace App\Tests\Cart\Behavior\Context;


use App\Tests\Cart\Utils\CartTestHelper;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use PHPUnit\Framework\Assert;

final class CartAssertContext implements Context
{
    private CartTestHelper $testHelper;

    public function __construct(CartTestHelper $testHelper)
    {
        $this->testHelper = $testHelper;
    }

    /**
     * @Then /^cart "([^"]*)" should have products:$/
     */
    public function cartShouldHaveProducts(string $cartAlias, TableNode $table)
    {
        $cart = $this->testHelper->findCartByAlias($cartAlias);
        Assert::assertEquals(count($cart->getItems()), $table->getIterator()->count());
    }

    /**
     * @Then /^cart "([^"]*)" total should be "([^"]*)"$/
     */
    public function cartTotalShouldBe(string $cartAlias, float $total)
    {
        $cart = $this->testHelper->findCartByAlias($cartAlias);
        Assert::assertEquals((float)$cart->getTotal(), $total * 100);
    }

    /**
     * @Then /^there should be a new cart$/
     */
    public function thereShouldBeANewCart()
    {
        $cart = $this->testHelper->findCartById($this->testHelper->getLastCreatedCartId());
        Assert::assertNotNull($cart, 'There is no cart');
        Assert::assertEmpty($cart->getItems(), 'Cart must be empty!');
        Assert::assertEquals(0, $cart->getTotal(), sprintf('Cart total must be 0 %d given', $cart->getTotal()));
    }


}
