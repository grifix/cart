<?php
declare(strict_types=1);

namespace App\Tests\Cart\Behavior\Context;


use App\Tests\Cart\Utils\CartTestHelper;
use Behat\Behat\Context\Context;
use Symfony\Component\Yaml\Yaml;

final class CartSetupContext implements Context
{
    private CartTestHelper $testHelper;

    public function __construct(CartTestHelper $testHelper)
    {
        $this->testHelper = $testHelper;
    }

    /**
     * @Given /^the cart items limit is "([^"]*)"$/
     */
    public function theCartItemsLimitIs(int $limit)
    {
        $this->testHelper->setCartItemsLimit($limit);
    }

    /**
     * @Given /^the cart "([^"]*)" has (\d+) items$/
     */
    public function theCartHasItems(string $cartAlias, int $numberOfItems)
    {
        $cart = $this->testHelper->getCartFixtureBuilder($cartAlias);
        for ($i = 0; $i < $numberOfItems; $i++) {
            $this->testHelper->createCartItemFixtureBuilder()->setCartId($cart->getId())->build();
        }
    }

    /**
     * @Given /^the cart "([^"]*)" has product "([^"]*)"$/
     */
    public function theCartHasProduct(string $cartAlias, string $productAlias)
    {
        $this->testHelper->addProductToCart($cartAlias, $productAlias);
    }

    /**
     * @Given /^there is a cart "([^"]*)"$/
     */
    public function thereIsACart(string $alias)
    {
        $this->testHelper->createCartFixtureBuilder()->build($alias);
    }

    /**
     * @Given /^the cart "([^"]*)" has products:$/
     */
    public function theCartHasProducts(string $cartAlias, string $products)
    {
        $products = Yaml::parse($products);
        foreach ($products as $productAlias) {
            $this->testHelper->addProductToCart($cartAlias, $productAlias);
        }
    }
}
