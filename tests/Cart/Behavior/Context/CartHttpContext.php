<?php
declare(strict_types=1);

namespace App\Tests\Cart\Behavior\Context;


use App\Tests\Cart\Utils\CartTestHelper;
use App\Tests\Shared\Utils\HttpClient\HttpClient;
use Behat\Behat\Context\Context;
use Symfony\Component\HttpFoundation\Response;

final class CartHttpContext implements Context
{
    private HttpClient $httpClient;

    private CartTestHelper $testHelper;

    public function __construct(HttpClient $httpClient, CartTestHelper $testHelper)
    {
        $this->httpClient = $httpClient;
        $this->testHelper = $testHelper;
    }

    /**
     * @Then /^I should get an error that i cannot add to cart "([^"]*)" more than "([^"]*)" items$/
     */
    public function iShouldGetAnErrorThatICannotAddToCartMoreThanItems(string $cartAlias, int $limit)
    {
        $this->httpClient->assertLastError(
            Response::HTTP_BAD_REQUEST,
            sprintf('Items limit (%d) for cart "%s" is exceed!', $limit,
                $this->testHelper->getCartFixtureBuilder($cartAlias)->getId())
        );
    }

    /**
     * @Then /^I should get and error that product "([^"]*)" is already added to cart "([^"]*)"$/
     */
    public function iShouldGetAndErrorThatProductIsAlreadyAddedToCart(string $productAlias, string $cartAlias)
    {
        $this->httpClient->assertLastError(
            Response::HTTP_BAD_REQUEST,
            sprintf(
                'Product "%s" had been already added to the cart "%s"!',
                $this->testHelper->getProductFixture($productAlias)->getId(),
                $this->testHelper->getCartFixtureBuilder($cartAlias)->getId()
            )
        );
    }

    /**
     * @When /^I add product to the cart "([^"]*)"$/
     */
    public function iAddProductToTheCart(string $cartAlias)
    {
        $cart = $this->testHelper->getCartFixtureBuilder($cartAlias);
        $this->performAddProductToCartRequest(
            $cart->getId(),
            $this->testHelper->createProductFixtureBuilder()->build()->getId()
        );
    }

    /**
     * @When /^I add product "([^"]*)" into the cart "([^"]*)"$/
     */
    public function iAddProductIntoTheCart(string $productAlias, string $cartAlias)
    {
        $this->performAddProductToCartRequest(
            $this->testHelper->getCartFixtureBuilder($cartAlias)->getId(),
            $this->testHelper->getProductFixture($productAlias)->getId()
        );
    }

    /**
     * @When /^I create new cart$/
     */
    public function iCreateNewCart()
    {
        $this->performCreateCartRequest();
        $response = $this->httpClient->getLastResponseArray();
        if (isset($response['id'])) {
            $this->testHelper->setLastCreatedCartId($response['id']);
        }
    }

    /**
     * @When /^I remove product "([^"]*)" from the cart "([^"]*)"$/
     */
    public function iRemoveProductFromTheCart(string $productAlias, string $cartAlias)
    {
        $this->performRemoveProductFromCartRequest(
            $this->testHelper->getCartFixtureBuilder($cartAlias)->getId(),
            $this->testHelper->getProductFixture($productAlias)->getId()
        );
    }

    /**
     * @When /^I remove product with id "([^"]*)" from the cart "([^"]*)"$/
     */
    public function iRemoveProductWithIdFromTheCart(string $productId, string $cartAlias)
    {
        $this->performRemoveProductFromCartRequest($this->testHelper->getCartFixtureBuilder($cartAlias)->getId(), $productId);
    }

    /**
     * @When /^I remove product "([^"]*)" from cart with id "([^"]*)"$/
     */
    public function iRemoveProductFromCartWithId(string $productAlias, string $cartId)
    {
        $this->performRemoveProductFromCartRequest($cartId, $this->testHelper->getProductFixture($productAlias)->getId());
    }

    /**
     * @Then /^I should gat an error that cart "([^"]*)" does not exist$/
     */
    public function iShouldGatAnErrorThatCartDoesNotExist(string $cartId)
    {
        $this->httpClient->assertLastError(
            Response::HTTP_NOT_FOUND,
            sprintf(
                'Cart "%s" does not exist!',
                $cartId,
            )
        );
    }

    private function performRemoveProductFromCartRequest(string $cartId, string $productId): void
    {
        $this->httpClient->delete(
            'cart_remove_product',
            [
                'cartId' => $cartId,
                'productId' => $productId
            ]
        );
    }

    private function performAddProductToCartRequest(string $cartId, string $productId): void
    {
        $this->httpClient->post(
            'cart_add_product',
            ['cartId' => $cartId],
            ['productId' => $productId]
        );
    }

    private function performCreateCartRequest(): void
    {
        $this->httpClient->post(
            'cart_create_cart',
        );
    }

    /**
     * @Then /^I should get an error that product with id "([^"]*)" does not exist in the cart "([^"]*)"$/
     */
    public function iShouldGetAnErrorThatProductWithIdDoesNotExistInTheCart(string $productId, string $cartAlias)
    {
        $this->httpClient->assertLastError(
            Response::HTTP_NOT_FOUND,
            sprintf(
                'Product "%s" does not exist in the cart "%s"!',
                $productId,
                $this->testHelper->getCartFixtureBuilder($cartAlias)->getId()
            )
        );
    }

}
