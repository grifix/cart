<?php

declare(strict_types=1);

namespace App\Tests\Cart\Utils\FixtureMother\Cart;


use App\Cart\Application\Command\Cart\CreateCart\CreateCartCommand;
use App\Shared\Application\CommandBusInterface;
use App\Shared\Domain\Uuid\UuidFactory;
use App\Tests\Shared\Utils\FixtureRegistry;

final class CartFixtureBuilder
{
    private string $id;

    public function __construct(
        private UuidFactory         $uuidFactory,
        private CommandBusInterface $commandBus,
        private FixtureRegistry     $fixtureRegistry

    )
    {
        $this->id = (string)$this->uuidFactory->createRandom();
    }

    public function setId(string $id): CartFixtureBuilder
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function build(?string $alias = null): self
    {
        $this->commandBus->executeCommand(new CreateCartCommand($this->id));
        if ($alias) {
            $this->fixtureRegistry->addFixture(clone $this, $alias);
        }
        return $this;
    }

}
