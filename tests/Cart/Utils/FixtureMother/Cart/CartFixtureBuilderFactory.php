<?php

declare(strict_types=1);

namespace App\Tests\Cart\Utils\FixtureMother\Cart;


use App\Shared\Application\CommandBusInterface;
use App\Shared\Domain\Uuid\UuidFactory;
use App\Tests\Shared\Utils\FixtureRegistry;

final class CartFixtureBuilderFactory
{
    public function __construct(
        private UuidFactory         $uuidFactory,
        private CommandBusInterface $commandBus,
        private FixtureRegistry     $fixtureRegistry
    )
    {
    }

    public function create(): CartFixtureBuilder
    {
        return new CartFixtureBuilder(
            $this->uuidFactory,
            $this->commandBus,
            $this->fixtureRegistry
        );
    }
}
