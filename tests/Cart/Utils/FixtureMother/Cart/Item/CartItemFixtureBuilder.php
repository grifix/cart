<?php

declare(strict_types=1);

namespace App\Tests\Cart\Utils\FixtureMother\Cart\Item;


use App\Cart\Application\Command\Cart\AddProduct\AddProductCommand;
use App\Cart\Application\Query\FindCart\FindCartQuery;
use App\Catalog\Application\Projection\ProductProjection\ProductFilter;
use App\Catalog\Application\Query\FindProducts\FindProductsQuery;
use App\Shared\Application\CommandBusInterface;
use App\Shared\Application\QueryBusInterface;
use App\Shared\Domain\Uuid\UuidFactory;
use App\Tests\Cart\Utils\FixtureMother\Cart\CartFixtureBuilderFactory;
use App\Tests\Catalog\Utils\FixtureMother\Product\ProductFixtureBuilderFactory;
use App\Tests\Shared\Utils\FixtureRegistry;

final class CartItemFixtureBuilder
{
    private string $productId;

    private string $cartId;

    public function __construct(
        private CommandBusInterface          $commandBus,
        private CartFixtureBuilderFactory    $cartFixtureBuilderFactory,
        private QueryBusInterface            $queryBus,
        private UuidFactory                  $uuidFactory,
        private FixtureRegistry              $fixtureRegistry,
        private ProductFixtureBuilderFactory $productFixtureBuilderFactory
    )
    {
        $this->productId = (string)$this->uuidFactory->createRandom();
        $this->cartId = (string)$this->uuidFactory->createRandom();
    }

    public function getProductId(): ?string
    {
        return $this->productId;
    }

    public function setProductId(?string $productId): CartItemFixtureBuilder
    {
        $this->productId = $productId;
        return $this;
    }

    public function getCartId(): ?string
    {
        return $this->cartId;
    }

    public function setCartId(?string $cartId): CartItemFixtureBuilder
    {
        $this->cartId = $cartId;
        return $this;
    }

    public function build(?string $alias = null): self
    {
        $this->createProductIfNotExists();
        $this->createCartIfNotExists();


        $this->commandBus->executeCommand(new AddProductCommand($this->cartId, $this->productId));
        if ($alias) {
            $this->fixtureRegistry->addFixture(clone $this, $alias);
        }

        return $this;
    }

    private function createProductIfNotExists(): void
    {
        $result = $this->queryBus->executeQuery(
            new FindProductsQuery(ProductFilter::create()->setProductId($this->productId))
        );

        if (empty($result->getProducts())) {
            $this->productFixtureBuilderFactory->create()->setId($this->productId)->build();
        }
    }

    private function createCartIfNotExists(): void
    {
        if (null === $this->queryBus->executeQuery(new FindCartQuery($this->cartId))->getCart()) {
            $this->cartFixtureBuilderFactory->create()->setId($this->cartId)->build();
        }
    }
}
