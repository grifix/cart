<?php

declare(strict_types=1);

namespace App\Tests\Cart\Utils\FixtureMother\Cart\Item;


use App\Shared\Application\CommandBusInterface;
use App\Shared\Application\QueryBusInterface;
use App\Shared\Domain\Uuid\UuidFactory;
use App\Tests\Cart\Utils\FixtureMother\Cart\CartFixtureBuilderFactory;
use App\Tests\Catalog\Utils\FixtureMother\Product\ProductFixtureBuilderFactory;
use App\Tests\Shared\Utils\FixtureRegistry;

final class CartItemFixtureBuilderFactory
{
    public function __construct(
        private CommandBusInterface       $commandBus,
        private CartFixtureBuilderFactory $cartFixtureBuilderFactory,
        private QueryBusInterface         $queryBus,
        private UuidFactory               $uuidFactory,
        private FixtureRegistry           $fixtureRegistry,
        private ProductFixtureBuilderFactory $productFixtureBuilderFactory
    )
    {

    }

    public function create(): CartItemFixtureBuilder
    {
        return new CartItemFixtureBuilder(
            $this->commandBus,
            $this->cartFixtureBuilderFactory,
            $this->queryBus,
            $this->uuidFactory,
            $this->fixtureRegistry,
            $this->productFixtureBuilderFactory
        );
    }
}

