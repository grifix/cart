<?php

declare(strict_types=1);

namespace App\Tests\Cart\Utils;


use App\Cart\Application\Command\Cart\AddProduct\AddProductCommand;
use App\Cart\Application\Dictionary\ConstraintsDictionary\ConstraintsDictionaryInterface;
use App\Cart\Application\Projection\CartProjection\CartDto;
use App\Cart\Application\Query\FindCart\FindCartQuery;
use App\Shared\Application\CommandBusInterface;
use App\Shared\Application\QueryBusInterface;
use App\Tests\Cart\Mock\ConstraintDictionaryMock;
use App\Tests\Cart\Utils\FixtureMother\Cart\CartFixtureBuilder;
use App\Tests\Cart\Utils\FixtureMother\Cart\CartFixtureBuilderFactory;
use App\Tests\Cart\Utils\FixtureMother\Cart\Item\CartItemFixtureBuilder;
use App\Tests\Cart\Utils\FixtureMother\Cart\Item\CartItemFixtureBuilderFactory;
use App\Tests\Catalog\Utils\CatalogTestHelper;
use App\Tests\Catalog\Utils\FixtureMother\Product\ProductFixtureBuilder;
use App\Tests\Shared\Utils\FixtureRegistry;

final class CartTestHelper
{
    private ?string $lastCreatedCartId = null;

    public function __construct(
        private QueryBusInterface              $queryBus,
        private CommandBusInterface            $commandBus,
        private FixtureRegistry                $fixtureRegistry,
        private CartFixtureBuilderFactory      $cartFixtureBuilderFactory,
        private CatalogTestHelper              $catalogTestHelper,
        private ConstraintsDictionaryInterface $constraintDictionary,
        private CartItemFixtureBuilderFactory $cartItemFixtureBuilderFactory
    ) {
    }

    public function getLastCreatedCartId(): ?string
    {
        return $this->lastCreatedCartId;
    }

    public function setLastCreatedCartId(?string $lastCreatedCartId): void
    {
        $this->lastCreatedCartId = $lastCreatedCartId;
    }

    public function findCartById(string $cartId): ?CartDto
    {
        return $this->queryBus->executeQuery(new FindCartQuery($cartId))->getCart();
    }

    public function findCartByAlias(string $alias): ?CartDto
    {
        return $this->findCartById($this->getCartFixtureBuilder($alias)->getId());
    }

    public function createCartFixtureBuilder(): CartFixtureBuilder
    {
        return $this->cartFixtureBuilderFactory->create();
    }

    public function getCartFixtureBuilder(string $alias): CartFixtureBuilder
    {
        return $this->fixtureRegistry->getFixture(CartFixtureBuilder::class, $alias);
    }

    public function getProductFixture(string $alias): ProductFixtureBuilder
    {
        return $this->catalogTestHelper->getProductFixtureBuilder($alias);
    }

    public function setCartItemsLimit(int $limit): void
    {
        $this->constraintDictionary->setMaxCartItemsLimit($limit);
    }

    public function createCartItemFixtureBuilder(): CartItemFixtureBuilder
    {
        return $this->cartItemFixtureBuilderFactory->create();
    }

    public function createProductFixtureBuilder(?ProductFixtureBuilder $fixture = null): ProductFixtureBuilder
    {
        return $this->catalogTestHelper->createProductFixtureBuilder();
    }

    public function addProductToCart(string $cartAlias, string $productAlias):void{
        $this->commandBus->executeCommand(
            new AddProductCommand(
                $this->getCartFixtureBuilder($cartAlias)->getId(),
                $this->getProductFixture($productAlias)->getId()
            )
        );
    }
}
