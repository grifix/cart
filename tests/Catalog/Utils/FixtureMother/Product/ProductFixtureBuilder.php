<?php


namespace App\Tests\Catalog\Utils\FixtureMother\Product;


use App\Catalog\Application\Command\Product\CreateProduct\CreateProductCommand;
use App\Shared\Application\CommandBusInterface;
use App\Shared\Domain\Money\MoneyFactory;
use App\Shared\Domain\Uuid\UuidFactory;
use App\Tests\Shared\Utils\FixtureRegistry;

final class ProductFixtureBuilder
{
    private string $id;

    private string $name;

    private int $priceAmount;

    private string $priceCurrency;

    public function __construct(
        private UuidFactory         $uuidFactory,
        private CommandBusInterface $commandBus,
        private FixtureRegistry     $fixtureRegistry,
        private MoneyFactory        $moneyFactory
    )
    {
        $this->id = (string)$this->uuidFactory->createRandom();
        $this->name = uniqid('product_');
        $this->priceAmount = 100;
        $this->priceCurrency = 'PLN';
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): ProductFixtureBuilder
    {
        $this->id = $id;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ProductFixtureBuilder
    {
        $this->name = $name;
        return $this;
    }

    public function getPriceAmount(): ?int
    {
        return $this->priceAmount;
    }

    public function setPriceAmount(?int $priceAmount): ProductFixtureBuilder
    {
        $this->priceAmount = $priceAmount;
        return $this;
    }

    public function getPriceCurrency(): ?string
    {
        return $this->priceCurrency;
    }

    public function setPriceCurrency(?string $priceCurrency): ProductFixtureBuilder
    {
        $this->priceCurrency = $priceCurrency;
        return $this;
    }

    public function build(?string $alias = null): self
    {
        $this->commandBus->executeCommand(new CreateProductCommand(
            $this->getId(),
            $this->getName(),
            $this->moneyFactory->create($this->getPriceAmount(), $this->getPriceCurrency())
        ));

        if ($alias) {
            $this->fixtureRegistry->addFixture(clone $this, $alias);
        }
        return $this;
    }
}
