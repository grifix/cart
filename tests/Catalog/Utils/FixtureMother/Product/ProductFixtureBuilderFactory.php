<?php
declare(strict_types=1);

namespace App\Tests\Catalog\Utils\FixtureMother\Product;


use App\Shared\Application\CommandBusInterface;
use App\Shared\Domain\Money\MoneyFactory;
use App\Shared\Domain\Uuid\UuidFactory;
use App\Tests\Shared\Utils\FixtureRegistry;

final class ProductFixtureBuilderFactory
{
    public function __construct(
        private UuidFactory         $uuidFactory,
        private CommandBusInterface $commandBus,
        private FixtureRegistry     $fixtureRegistry,
        private MoneyFactory        $moneyFactory
    )
    {
    }

    public function create(): ProductFixtureBuilder
    {
        return new ProductFixtureBuilder(
            $this->uuidFactory,
            $this->commandBus,
            $this->fixtureRegistry,
            $this->moneyFactory
        );
    }
}
