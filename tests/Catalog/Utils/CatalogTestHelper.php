<?php


namespace App\Tests\Catalog\Utils;


use App\Catalog\Application\Command\Product\DeleteProduct\DeleteProductCommand;
use App\Catalog\Application\Dictionary\ProductNameConstraintDictionary\ConstraintsDictionaryInterface;
use App\Catalog\Application\Projection\ProductProjection\ProductDto;
use App\Catalog\Application\Projection\ProductProjection\ProductFilter;
use App\Catalog\Application\Query\FindProducts\FindProductsQuery;
use App\Catalog\Application\Query\FindProducts\FindProductsQueryResult;
use App\Shared\Application\CommandBusInterface;
use App\Shared\Application\QueryBusInterface;
use App\Tests\Catalog\Mock\ConstraintsDictionaryMock;
use App\Tests\Catalog\Utils\FixtureMother\Product\ProductFixtureBuilder;
use App\Tests\Catalog\Utils\FixtureMother\Product\ProductFixtureBuilderFactory;
use App\Tests\Shared\Utils\FixtureRegistry;

final class CatalogTestHelper
{

    private ?string $lastCreatedProductId = null;

    /**
     * @param ConstraintsDictionaryMock $constraintDictionary
     */
    public function __construct(
        private QueryBusInterface              $queryBus,
        private CommandBusInterface            $commandBus,
        private ProductFixtureBuilderFactory   $productFixtureBuilderFactory,
        private FixtureRegistry                $fixtureRegistry,
        private ConstraintsDictionaryInterface $constraintDictionary
    ) {
    }

    public function getLastCreatedProductId(): ?string
    {
        return $this->lastCreatedProductId;
    }

    public function setLastCreatedProductId(?string $lastCreatedProductId): void
    {
        $this->lastCreatedProductId = $lastCreatedProductId;
    }

    public function findProduct(ProductFilter $filter): ?ProductDto
    {
        $result = $this->queryBus->executeQuery(new FindProductsQuery($filter));
        if (count($result->getProducts())) {
            return $result->getProducts()[0];
        }
        return null;
    }

    public function findProductByAlias(string $productAlias): ?ProductDto
    {
        return $this->findProduct(ProductFilter::create()->setProductId($this->getProductFixtureBuilder($productAlias)->getId()));
    }

    public function createProductFixtureBuilder(): ProductFixtureBuilder
    {
        return $this->productFixtureBuilderFactory->create();
    }

    public function getProductFixtureBuilder(string $alias): ProductFixtureBuilder
    {
        return $this->fixtureRegistry->getFixture(ProductFixtureBuilder::class, $alias);
    }

    public function deleteProduct(string $alias): void
    {
        $this->commandBus->executeCommand(new DeleteProductCommand($this->getProductFixtureBuilder($alias)->getId()));
    }

    public function setProductNameMaxLength(int $maxLength): void
    {
        $this->constraintDictionary->setProductNameMaxLength($maxLength);
    }

    public function setProductNameMinLength(int $minLength): void
    {
        $this->constraintDictionary->setProductNameMinLength($minLength);
    }

    public function setMaxProductsOnPage(int $maxProductsOnPage): void
    {
        $this->constraintDictionary->setMaxProductsOnPage($maxProductsOnPage);
    }
}
