<?php

declare(strict_types=1);

namespace App\Tests\Catalog\Mock;

use App\Catalog\Application\Dictionary\ProductNameConstraintDictionary\ConstraintsDictionaryInterface;
use App\Catalog\Application\Dictionary\ProductNameConstraintDictionary\ProductNameConstraintsDto;

final class ConstraintsDictionaryMock implements ConstraintsDictionaryInterface
{

    private int $productNameMinLength;

    private int $productNameMaxLength;

    private int $maxProductsOnPage;

    public function __construct(
        int $productNameMinLength,
        int $productNameMaxLength,
        int $maxProductsOnPage
    )
    {
        $this->productNameMinLength = $productNameMinLength;
        $this->productNameMaxLength = $productNameMaxLength;
        $this->maxProductsOnPage = $maxProductsOnPage;
    }

    public function setProductNameMinLength(int $productNameMinLength): void
    {
        $this->productNameMinLength = $productNameMinLength;
    }

    public function setProductNameMaxLength(int $productNameMaxLength): void
    {
        $this->productNameMaxLength = $productNameMaxLength;
    }

    public function getProductNameMinLength(): int
    {
        return $this->productNameMinLength;
    }

    public function getProductNameMaxLength(): int
    {
        return $this->productNameMaxLength;
    }

    public function getProductNameConstraints(): ProductNameConstraintsDto
    {
        return new ProductNameConstraintsDto($this->productNameMaxLength, $this->productNameMinLength);
    }

    /**
     * @return int
     */
    public function getMaxProductsOnPage(): int
    {
        return $this->maxProductsOnPage;
    }

    public function setMaxProductsOnPage(int $maxProductsOnPage): void
    {
        $this->maxProductsOnPage = $maxProductsOnPage;
    }
}
