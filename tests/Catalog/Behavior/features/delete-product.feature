Feature: Delete product
  Background:
    Given supported currencies are:
    """
    - PLN
    """

  Scenario: Delete product
    Given there is a product "Notebook"
    When I delete product "Notebook"
    Then There should be no product "Notebook"

  Scenario: Delete product twice
    Given there is a product "Notebook"
    When I delete product "Notebook"
    And I delete product "Notebook"
    Then I should get an error that product "Notebook" does not exist

  Scenario: Delete non existed product
    When I delete product with id "1263b14e-1869-44c6-92c0-97f5354332ec"
    Then I should get an error that product with id "1263b14e-1869-44c6-92c0-97f5354332ec" does not exists
