Feature: Create product
  Background:
    Given supported currencies are:
    """
    - PLN
    """

  Scenario: Create product
    When I create product:
    """
    name: Test product
    price:
      amount: 1125
      currency: PLN
    """
    Then there should be the product:
    """
    name: Test product
    price:
      amount: 1125
      currency: PLN
    """

  Scenario: Create product with existed name
    Given there is a product with name "Laptop"
    When I create product with name "Laptop"
    Then I should get and error that product with this name "Laptop" already exists

  Scenario: Create product with too long name
    Given product name max lengths is 4
    When I create product with name "Laptop"
    Then I should get an error that product name should have at least 4 characters

  Scenario: Create product with wrong currency
    When I create product with price currency "RUB"
    Then I should get an error that currency is not supported



