Feature: Find product

  Background:
    Given supported currencies are:
    """
    - PLN
    """
    And there are products:
      | name                          | price | currency |
      | The Godfather                 | 59.99 | PLN      |
      | Steve Jobs                    | 49.95 | PLN      |
      | The Return of Sherlock Holmes | 39.99 | PLN      |
      | The Little Prince             | 29.99 | PLN      |
      | I Hate Myselfie!              | 19.99 | PLN      |
      | The Trial                     | 9.99  | PLN      |
  And the products limit on page is "3"

  Scenario: list products from page 1
    When I list all products from page "1"
    Then I should get products:
      | name                          | price | currency |
      | The Godfather                 | 59.99 | PLN      |
      | Steve Jobs                    | 49.95 | PLN      |
      | The Return of Sherlock Holmes | 39.99 | PLN      |

  Scenario: list products from page 2
    When I list all products from page "2"
    Then I should get products:
      | name                          | price | currency |
      | The Little Prince             | 29.99 | PLN      |
      | I Hate Myselfie!              | 19.99 | PLN      |
      | The Trial                     | 9.99  | PLN      |
