Feature: Update product
  Background:
    Given supported currencies are:
    """
    - PLN
    """
    And there is a product "Notebook" with params:
    """
    name: Notebook
    price:
      amount: 500
      currency: PLN
    """

  Scenario: Update product
    When I update product "Notebook":
    """
    name: Laptop
    price:
      amount: 200
    """
    Then product "Notebook" should be:
    """
    name: Laptop
    price:
      amount: 200
    """

  Scenario: Update only name
    When I update product "Notebook":
    """
    name: Laptop
    """
    Then product "Notebook" should be:
    """
    name: Laptop
    price:
      amount: 500
    """

  Scenario: Update non-existed product
    When I update product with id "1263b14e-1869-44c6-92c0-97f5354332ec"
    Then I should get an error that product with id "1263b14e-1869-44c6-92c0-97f5354332ec" does not exists

  Scenario: Update deleted product
    Given product "Notebook" is deleted
    When I update product "Notebook":
    """
    name: New name
    """
    Then I should get an error that product "Notebook" does not exist
