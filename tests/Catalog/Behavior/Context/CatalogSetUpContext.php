<?php
declare(strict_types=1);

namespace App\Tests\Catalog\Behavior\Context;


use App\Shared\Infrastructure\Common\ArrayAccessor;
use App\Tests\Catalog\Utils\CatalogTestHelper;
use App\Tests\Catalog\Utils\FixtureMother\Product\ProductFixtureBuilder;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Symfony\Component\Yaml\Yaml;

final class CatalogSetUpContext implements Context
{

    private CatalogTestHelper $testHelper;

    public function __construct(CatalogTestHelper $testHelper)
    {
        $this->testHelper = $testHelper;
    }

    /**
     * @Given there is a product with name :name
     */
    public function thereIsAProductWithName(string $name)
    {
        $this->testHelper->createProductFixtureBuilder()->setName($name)->build();
    }

    /**
     * @Given there is a product :alias with params:
     */
    public function thereIsAProductWithParams(string $alias, string $product)
    {
        $product = new ArrayAccessor(Yaml::parse($product));

        $this->testHelper->createProductFixtureBuilder()
            ->setName($product->get('name'))
            ->setPriceAmount($product->get('price.amount'))
            ->setPriceCurrency($product->get('price.currency'))
            ->build($alias)
        ;

    }

    /**
     * @Given /^there are products:$/
     */
    public function thereAreProducts(TableNode $products)
    {
        foreach ($products as $product) {
            $this->testHelper->createProductFixtureBuilder()
                ->setName($product['name'])
                ->setPriceAmount((int)round($product['price'] * 100))
                ->setPriceCurrency($product['currency'])
                ->build($product['name']);
        }
    }

    /**
     * @Given /^the products limit on page is "([^"]*)"$/
     */
    public function theProductsLimitOnPageIs(int $limit)
    {
        $this->testHelper->setMaxProductsOnPage($limit);
    }

    /**
     * @Given product name max lengths is :maxLength
     */
    public function productNameMaxLengthsIs(int $maxLength)
    {
        $this->testHelper->setProductNameMaxLength($maxLength);
    }

    /**
     * @Given /^there is a product "([^"]*)"$/
     */
    public function thereIsAProduct(string $alias)
    {
        $this->testHelper->createProductFixtureBuilder()->setName($alias)->build($alias);
    }

    /**
     * @Given /^product "([^"]*)" is deleted$/
     */
    public function productIsDeleted(string $alias)
    {
        $this->testHelper->deleteProduct($alias);
    }
}
