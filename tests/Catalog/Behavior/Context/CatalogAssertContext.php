<?php
declare(strict_types=1);

namespace App\Tests\Catalog\Behavior\Context;


use App\Catalog\Application\Projection\ProductProjection\ProductFilter;
use App\Shared\Infrastructure\Common\ArrayAccessor;
use App\Tests\Catalog\Utils\CatalogTestHelper;
use Behat\Behat\Context\Context;
use PHPUnit\Framework\Assert;
use Symfony\Component\Yaml\Yaml;

final class CatalogAssertContext implements Context
{

    private CatalogTestHelper $testHelper;

    public function __construct(CatalogTestHelper $testHelper)
    {
        $this->testHelper = $testHelper;
    }

    /**
     * @Then /^there should be the product:$/
     */
    public function thereShouldBeTheProduct(string $expectedProduct)
    {
        $expectedProduct = Yaml::parse($expectedProduct);
        $actualProduct = $this->testHelper->findProduct(ProductFilter::create()->setProductId($this->testHelper->getLastCreatedProductId()));
        Assert::assertNotNull($actualProduct, 'Product not found');
        Assert::assertEquals($expectedProduct['name'], $actualProduct->getName());
        Assert::assertEquals($expectedProduct['price']['amount'], $actualProduct->getPrice()->getAmount());
        Assert::assertEquals($expectedProduct['price']['currency'], $actualProduct->getPrice()->getCurrencyCode());
    }

    /**
     * @Then /^There should be no product "([^"]*)"$/
     */
    public function thereShouldBeNoProduct(string $alias)
    {
        Assert::assertNull($this->testHelper->findProductByAlias($alias));
    }

    /**
     * @Then /^product "([^"]*)" should be:$/
     */
    public function productShouldBe(string $productAlias, string $expectedProduct)
    {
        $expectedProduct = new ArrayAccessor(Yaml::parse($expectedProduct));
        $actualProduct = $this->testHelper->findProductByAlias($productAlias);
        false === $expectedProduct->has('id') ?:
            Assert::assertEquals($expectedProduct->get('id'), $actualProduct->getId(), 'Wrong id');
        false === $expectedProduct->has('name') ?:
            Assert::assertEquals($expectedProduct->get('name'), $actualProduct->getName(), 'Wrong name');
        false === $expectedProduct->has('price.amount') ?:
            Assert::assertEquals($expectedProduct->get('price.amount'), $actualProduct->getPrice()->getAmount(),
                'Wrong price amount');
        false === $expectedProduct->has('price.currency') ?: Assert::assertEquals($expectedProduct->get('price.currency',
            'Wrong price currency'),
            $actualProduct->getPrice()->getCurrencyCode());
    }
}
