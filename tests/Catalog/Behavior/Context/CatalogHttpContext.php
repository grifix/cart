<?php
declare(strict_types=1);

namespace App\Tests\Catalog\Behavior\Context;


use App\Tests\Catalog\Utils\CatalogTestHelper;
use App\Tests\Shared\Utils\HttpClient\HttpClient;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

final class CatalogHttpContext implements Context
{
    private CatalogTestHelper $testHelper;

    private HttpClient $httpClient;

    /**
     * CatalogHttpContext constructor.
     * @param CatalogTestHelper $testHelper
     * @param HttpClient $httpClient
     */
    public function __construct(CatalogTestHelper $testHelper, HttpClient $httpClient)
    {
        $this->testHelper = $testHelper;
        $this->httpClient = $httpClient;
    }

    /**
     * @When /^I create product:$/
     */
    public function iCreateProduct(string $product)
    {
        $this->performCreateProductRequest(Yaml::parse($product));
        $response = $this->httpClient->getLastResponseArray();
        if (isset($response['id'])) {
            $this->testHelper->setLastCreatedProductId($response['id']);
        }
    }

    /**
     * @When I create product with name :name
     */
    public function iCreateProductWithName(string $name)
    {
        $this->performCreateProductRequest([
            'name' => $name
        ]);
    }

    /**
     * @Then I should get and error that product with this name :name already exists
     */
    public function iShouldGetAndErrorThatProductWithThisNameAlreadyExists(string $name)
    {
        $this->httpClient->assertLastError(
            Response::HTTP_BAD_REQUEST,
            sprintf('Product with name %s already exists!', $name)
        );
    }

    /**
     * @When /^I create product with price currency "([^"]*)"$/
     */
    public function iCreateProductWithPriceCurrency(string $currency)
    {
        $this->performCreateProductRequest([
            'price' => [
                'currency' => $currency
            ]
        ]);
    }

    /**
     * @Then /^I should get an error that currency is not supported$/
     */
    public function iShouldGetAnErrorThatCurrencyIsNotSupported()
    {
        $this->httpClient->assertLastErrorResponse(
            Response::HTTP_BAD_REQUEST,
            ['[price][currency]' => 'The value you selected is not a valid choice.']
        );
    }

    /**
     * @Then /^I should get an error that product name should have at least (\d+) characters$/
     */
    public function iShouldGetAnErrorThatProductShouldHaveAtLeastCharacters(int $minLength)
    {
        $this->httpClient->assertLastErrorResponse(
            Response::HTTP_BAD_REQUEST,
            [
                '[name]' => sprintf(
                    'This value is too long. It should have %d characters or less.',
                    $minLength
                )
            ]
        );
    }

    /**
     * @When /^I delete product "([^"]*)"$/
     */
    public function iDeleteProduct(string $alias)
    {
        $this->performDeleteProductRequest($this->testHelper->getProductFixtureBuilder($alias)->getId());
    }

    /**
     * @When I delete product with id :id
     */
    public function iDeleteProductWithId(string $id)
    {
        $this->performDeleteProductRequest($id);
    }

    /**
     * @When /^I list all products from page "([^"]*)"$/
     */
    public function iListAllProductsFromPage(int $page)
    {
        $this->performFindProductRequest($page);
    }

    /**
     * @Then /^I should get products:$/
     */
    public function iShouldGetProducts(TableNode $table)
    {
        $response = $this->httpClient->getLastResponseArray();
        Assert::assertCount($table->getIterator()->count(), $response);
        foreach ($table as $i => $expectedProduct){
            $actualProduct = $response[$i];
            Assert::assertEquals($expectedProduct['name'], $actualProduct['name']);
            Assert::assertEquals($expectedProduct['price'], $actualProduct['price']['amount']);
            Assert::assertEquals($expectedProduct['currency'], $actualProduct['price']['currencyCode']);
        }
    }

    /**
     * @When /^I update product "([^"]*)":$/
     */
    public function iUpdateProduct(string $productAlias, string $product)
    {
        $this->performUpdateProductRequest(
            $this->testHelper->getProductFixtureBuilder($productAlias)->getId(),
            Yaml::parse($product)
        );
    }

    /**
     * @When I update product with id :id
     */
    public function iUpdateProductWithId(string $id)
    {
        $this->performUpdateProductRequest($id, ['name' => 'Test']);
    }

    /**
     * @Then I should get an error that product with id :id does not exists
     */
    public function iShouldGetAnErrorThatProductWithIdDoesNotExists(string $id)
    {
        $this->httpClient->assertLastError(
            Response::HTTP_NOT_FOUND,
            sprintf('Product with id %s does not exist!', $id)
        );
    }

    /**
     * @Then /^I should get an error that product "([^"]*)" does not exist$/
     */
    public function iShouldGetAnErrorThatProductDoesNotExist(string $alias)
    {
        $this->httpClient->assertLastError(
            Response::HTTP_NOT_FOUND,
            sprintf('Product with id %s does not exist!', $this->testHelper->getProductFixtureBuilder($alias)->getId())
        );
    }


    private function performFindProductRequest(?int $page = null): void
    {
        $query = [];
        null === $page ?: $query['page'] = $page;
        $this->httpClient->get(
            'catalog_find_products',
            [],
            $query
        );
    }

    private function performDeleteProductRequest(string $productId): void
    {
        $this->httpClient->delete(
            'catalog_delete_product',
            ['productId' => $productId]
        );
    }

    private function performCreateProductRequest(array $product): void
    {
        $this->httpClient->post(
            'catalog_create_product',
            [],
            $this->prepareProductPayload($product)
        );
    }

    private function prepareProductPayload(array $product): array
    {
        $defaults = [
            'name' => uniqid('product_'),
            'price' => [
                'amount' => 100,
                'currency' => 'PLN'
            ]
        ];

        return array_replace_recursive($defaults, $product);
    }

    private function performUpdateProductRequest(string $productId, array $product): void
    {
        $this->httpClient->patch(
            'catalog_update_product',
            ['productId' => $productId],
            $product
        );
    }




}
