<?php

namespace App\Tests\Catalog\Unit\Product;

use App\Catalog\Domain\Product\Events\ProductCreatedEvent;
use App\Catalog\Domain\Product\Events\ProductDeletedEvent;
use App\Catalog\Domain\Product\Events\ProductPriceChangedEvent;
use App\Catalog\Domain\Product\Events\ProductRenamedEvent;
use App\Catalog\Domain\Product\Exceptions\ProductDeletedException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameAlreadyExistsException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameTooLongException;
use App\Catalog\Domain\Product\Name\Exception\ProductNameTooShortException;
use App\Catalog\Domain\Product\Product;
use App\Shared\Domain\Money\Currency\Currency;
use App\Tests\Catalog\Unit\Product\Builders\ProductObjectBuilder;
use App\Tests\Catalog\Unit\Product\Builders\ProductOutsideMockBuilder;
use App\Tests\Shared\Unit\Money\Builders\MoneyObjectBuilder;
use App\Tests\Shared\Unit\Money\Currency\Builders\CurrencyObjectBuilder;
use App\Tests\Shared\Unit\Uuid\Builders\UuidObjectBuilder;
use PHPUnit\Framework\TestCase;

final class ProductTest extends TestCase
{

    public function testItCreates(): void
    {
        $outsideBuilder = ProductOutsideMockBuilder::create();
        new Product(
            $outsideBuilder->build(),
            UuidObjectBuilder::buildWithValue('3f563b2c-a0f9-4757-a0fa-bcb92af4f1ad'),
            'TestProduct',
            MoneyObjectBuilder::buildWithAmountAndCurrency('100', Currency::CODE_USD)
        );

        self::assertCount(1, $outsideBuilder->getPublishedEvents());
        self::assertEquals(
            new ProductCreatedEvent(
                UuidObjectBuilder::buildWithValue('3f563b2c-a0f9-4757-a0fa-bcb92af4f1ad'),
                'TestProduct',
                MoneyObjectBuilder::buildWithAmountAndCurrency('100', Currency::CODE_USD)
            ),
            $outsideBuilder->getPublishedEvents()[0],
        );
    }

    public function testItRenames(): void
    {
        $outsideBuilder = ProductOutsideMockBuilder::create();
        $product = ProductObjectBuilder::create()
            ->setOutside($outsideBuilder->build())
            ->setId(UuidObjectBuilder::buildWithValue('3f563b2c-a0f9-4757-a0fa-bcb92af4f1ad'))
            ->build();
        $outsideBuilder->clearPublishedEvents();

        $product->rename('new name');
        self::assertCount(1, $outsideBuilder->getPublishedEvents());
        self::assertEquals(
            new ProductRenamedEvent(
                UuidObjectBuilder::buildWithValue('3f563b2c-a0f9-4757-a0fa-bcb92af4f1ad'),
                'new name'
            ),
            $outsideBuilder->getPublishedEvents()[0]
        );
    }

    public function testItCannotRenameDeleted(): void
    {
        $product = ProductObjectBuilder::create()->build();
        $product->delete();
        $this->expectException(ProductDeletedException::class);
        $product->rename('new name');
    }

    public function testItChangesPrice(): void
    {

        $outsideBuilder = ProductOutsideMockBuilder::create();
        $product = ProductObjectBuilder::create()
            ->setOutside($outsideBuilder->build())
            ->setId(UuidObjectBuilder::buildWithValue('3f563b2c-a0f9-4757-a0fa-bcb92af4f1ad'))
            ->build();
        $outsideBuilder->clearPublishedEvents();

        $product->changePrice(200);
        self::assertCount(1, $outsideBuilder->getPublishedEvents());
        self::assertEquals(
            new ProductPriceChangedEvent(
                UuidObjectBuilder::buildWithValue('3f563b2c-a0f9-4757-a0fa-bcb92af4f1ad'),
                MoneyObjectBuilder::buildWithAmount(200)
            ),
            $outsideBuilder->getPublishedEvents()[0]
        );
    }

    public function testItCannotChangePriceOnDeleted(): void
    {
        $product = ProductObjectBuilder::create()->build();
        $product->delete();
        $this->expectException(ProductDeletedException::class);
        $product->changePrice(MoneyObjectBuilder::buildWithAmount(200));
    }

    public function testItDeletes(): void
    {
        $outsideBuilder = ProductOutsideMockBuilder::create();
        $product = ProductObjectBuilder::create()
            ->setOutside($outsideBuilder->build())
            ->setId(UuidObjectBuilder::buildWithValue('3f563b2c-a0f9-4757-a0fa-bcb92af4f1ad'))
            ->build();
        $outsideBuilder->clearPublishedEvents();
        $product->delete();
        self::assertCount(1, $outsideBuilder->getPublishedEvents());
        self::assertEquals(
            new ProductDeletedEvent(UuidObjectBuilder::buildWithValue('3f563b2c-a0f9-4757-a0fa-bcb92af4f1ad')),
            $outsideBuilder->getPublishedEvents()[0]
        );
    }

    public function testItCannotDeleteTwice(): void
    {
        $product = ProductObjectBuilder::create()->build();
        $product->delete();
        $this->expectException(ProductDeletedException::class);
        $product->delete();
    }

    public function testItDoesNotCreateWithDuplicatedName(): void
    {
        $this->expectException(ProductNameAlreadyExistsException::class);
        ProductObjectBuilder::create()
            ->setOutside(ProductOutsideMockBuilder::create()->setProductNameExistsResult(true)->build())
            ->setName('New name')
            ->build();
    }

    public function testItDoesNotCreateIfNameIsTooShort(): void
    {
        $this->expectException(ProductNameTooShortException::class);
        ProductObjectBuilder::create()
            ->setOutside(ProductOutsideMockBuilder::create()->setGetMinProductNameLengthResult(5)->build())
            ->setName('1234')
            ->build();
    }

    public function testItDoesNotCreateIfNameIsTooLong(): void
    {
        $this->expectException(ProductNameTooLongException::class);
        ProductObjectBuilder::create()
            ->setOutside(ProductOutsideMockBuilder::create()->setGetMaxProductNameLengthResult(5)->build())
            ->setName('123456')
            ->build();
    }
}
