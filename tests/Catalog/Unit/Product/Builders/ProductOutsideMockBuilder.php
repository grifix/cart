<?php
declare(strict_types=1);

namespace App\Tests\Catalog\Unit\Product\Builders;


use App\Catalog\Domain\Product\ProductOutsideInterface;
use App\Tests\Shared\Utils\EventCollectorTrait;
use Mockery;
use Mockery\MockInterface;

final class ProductOutsideMockBuilder
{

    use EventCollectorTrait;

    private function __construct(
        private bool $productNameExistsResult = false,
        private int  $getMaxProductNameLengthResult = 255,
        private int  $getMinProductNameLengthResult = 3
    )
    {
    }

    public static function create(): self
    {
        return new self();
    }

    public function setProductNameExistsResult(bool $productNameExistsResult): self
    {
        $this->productNameExistsResult = $productNameExistsResult;
        return $this;
    }

    public function setGetMaxProductNameLengthResult(int $getMaxProductNameLengthResult): self
    {
        $this->getMaxProductNameLengthResult = $getMaxProductNameLengthResult;
        return $this;
    }

    public function setGetMinProductNameLengthResult(int $getMinProductNameLengthResult): self
    {
        $this->getMinProductNameLengthResult = $getMinProductNameLengthResult;
        return $this;
    }

    public function build(): MockInterface|ProductOutsideInterface
    {
        $result = Mockery::mock(ProductOutsideInterface::class);

        $this->enableEventCollector($result);

        $result->shouldReceive('productNameExists')->andReturn($this->productNameExistsResult);
        $result->shouldReceive('getMaxProductNameLength')->andReturn($this->getMaxProductNameLengthResult);
        $result->shouldReceive('getMinProductNameLength')->andReturn($this->getMinProductNameLengthResult);

        return $result;
    }
}
