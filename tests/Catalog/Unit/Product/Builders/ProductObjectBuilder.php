<?php
declare(strict_types=1);

namespace App\Tests\Catalog\Unit\Product\Builders;


use App\Catalog\Domain\Product\Product;
use App\Catalog\Domain\Product\ProductOutsideInterface;
use App\Shared\Domain\Money\Money;
use App\Shared\Domain\Uuid\Uuid;
use App\Tests\Shared\Unit\Money\Builders\MoneyObjectBuilder;
use App\Tests\Shared\Unit\Uuid\Builders\UuidObjectBuilder;

final class ProductObjectBuilder
{
    private ProductOutsideInterface $outside;

    private Uuid $id;

    private Money $price;

    private string $name = 'Product name';

    private function __construct()
    {
        $this->outside = ProductOutsideMockBuilder::create()->build();
        $this->id = UuidObjectBuilder::create()->build();
        $this->price = MoneyObjectBuilder::create()->build();
    }

    public static function create(): self
    {
        return new self();
    }

    public function setOutside(ProductOutsideInterface $outside): self
    {
        $this->outside = $outside;
        return $this;
    }

    public function setId(Uuid $id): ProductObjectBuilder
    {
        $this->id = $id;
        return $this;
    }

    public function setPrice(Money $price): self
    {
        $this->price = $price;
        return $this;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function build(): Product
    {
        return new Product(
            $this->outside,
            $this->id,
            $this->name,
            $this->price
        );
    }
}
