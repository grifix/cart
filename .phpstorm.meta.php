<?php

namespace PHPSTORM_META {

    use App\Shared\Application\QueryBusInterface;
    use App\Shared\Ui\Http\Responder\AbstractResponder;
    use App\Tests\Shared\Util\FixtureRegistry;

    override(
        FixtureRegistry::getFixture(0),
        map(['' => '@'])
    );

    override(
        QueryBusInterface::executeQuery($query),
        map([
            '' => '@Result'
        ])
    );

    override(
        AbstractResponder::executeQuery($query),
        map([
            '' => '@Result'
        ])
    );
}
